﻿
using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Configuration;
using System.Xml.Serialization;

public class Util
{
    public static T DeserializeToObject<T>(string xml) where T : class
    {
        System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(typeof(T));
       
            using (StringReader sr = new StringReader(xml))
            {
                return (T)ser.Deserialize(sr);

            }
    }
       
        
    
}

