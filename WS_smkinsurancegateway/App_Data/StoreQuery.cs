﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace WS_smkinsurancegateway.App_Data
{
    public class StoreQuery
    {
        public static string constr = System.Configuration.ConfigurationManager.ConnectionStrings["SMKNonMotorConnectionString"].ConnectionString;

        public static DataTable ImportPolicy_DataDetail(string xml)
        {
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(constr))
            using (SqlCommand cmd = new SqlCommand("mar_ins_reg_bigc", conn))
            using (SqlDataAdapter da = new SqlDataAdapter(cmd))
            {
                conn.Open();
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandTimeout = 600;
                cmd.Parameters.AddWithValue("@xml", xml);
                da.Fill(dt);
                conn.Close();
            }


            return dt;

        }


    }

}