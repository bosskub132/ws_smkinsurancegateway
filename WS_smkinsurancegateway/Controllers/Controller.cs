﻿using System;
using System.Web;
using System.Data;
using WS_smkinsurancegateway.Models;
using System.Linq;
using System.Collections.Generic;
using System.Configuration;
using System.Web.Http;
using WS_smkinsurancegateway.App_Data;
using System.Net.Http;
using System.Xml;
using System.Text;
using System.Xml.Serialization;
using System.IO;
using System.Xml.Schema;
using System.Net.Http.Headers;

namespace WS_smkinsurancegateway.Controllers
{

    
    public class SMKController : ApiController
    {
        //Check Bearer token
        private bool checkAuth()
        {
            System.Net.Http.Headers.HttpRequestHeaders headers = this.Request.Headers;
            string authString = ConfigurationManager.AppSettings["AuthString"];
            string token = string.Empty;
            if (headers.Contains("Authorization"))
            {
                if (headers.Authorization.Scheme.Equals("Bearer") && headers.Authorization.Parameter != null)
                {
                    token = headers.Authorization.Parameter.ToString().Trim();
                    if (token.Equals(authString))
                    {
                        return true;
                    }
                    else
                    {
                        return false;

                    }

                }
                else
                    return false;


            }
            //code to authenticate and return some thing
            else
            { return false; }
        }

        private string ValidateXml(string xml)
        {
            //namespace xml ต้องเป็นชื่อเดียวกัน
            XmlDocument doc = new XmlDocument();
            string schema = String.Format(@"C:\Users\SMK\Desktop\ApiInsuranceGateway\XSD\1. Import Policy Request and Confirm Message V3.1.xsd");
            doc.Load(schema);
            //var sss = doc.DocumentElement.NamespaceURI;
            string ns = doc.DocumentElement.Attributes["targetNamespace"].Value;
            var settings = new XmlReaderSettings();
            var xmlIsValid = true;
            var errorMsg = String.Empty;
            settings.Schemas.Add(ns, schema);
            settings.ValidationType = ValidationType.Schema;
            settings.ValidationEventHandler += (sender, args) =>
             {
                 xmlIsValid = false;
                 errorMsg = args.Message;
             };
            using (var s = new StringReader(xml))
            {
                var xr = XmlReader.Create(s, settings);
                while (xr.Read()) { }
                if (!xmlIsValid)
                {
                    return String.Format("XML file  is not valid: {0}", errorMsg);
                } 
            }
            return errorMsg;
        }

            //ImportPolicy
        [Route("ImportPolicy")]
        [HttpPost]
        public IHttpActionResult ImportPolicy_POST(HttpRequestMessage req_data)
        {
            if (!checkAuth())
            {
                //RegisterAndClaim_response dr = new RegisterAndClaim_response();
                //dr.result = "001";
                //dr.message = "Invalid token";
                //return Content(System.Net.HttpStatusCode.BadRequest, dr);
            }

            var xml = req_data.Content.ReadAsStringAsync().Result;
            //ValidateXml(xml);
            string errMsg = ValidateXml(xml);
            if (!errMsg.Equals(""))
            {
                return Content(System.Net.HttpStatusCode.BadRequest,errMsg);
            }
            CustomsImportDeclaration rr = new CustomsImportDeclaration();
            rr = Util.DeserializeToObject<CustomsImportDeclaration>(xml);

            try
            {
                //var json = new JavaScriptSerializer().Serialize(req_data);

                DataTable responseData = StoreQuery.ImportPolicy_DataDetail(xml);
                if (responseData.Rows.Count > 0)
                {
                    //PolicyValidate_response dr = new PolicyValidate_response();

                    //dr.result = responseData.Rows[0]["result"].ToString().Trim();
                    //dr.message = responseData.Rows[0]["message"].ToString().Trim();
                    //dr.reason = responseData.Rows[0]["reason"].ToString().Trim();
                    //dr.tracking_no = responseData.Rows[0]["tracking_no"].ToString().Trim();

                    return Ok();
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}