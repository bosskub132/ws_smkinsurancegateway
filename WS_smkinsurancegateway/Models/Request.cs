﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;
using System.ComponentModel.DataAnnotations;

namespace WS_smkinsurancegateway.Models
{

	[XmlRoot(ElementName = "BrokerInformation")]
	public class BrokerInformation
	{

		[XmlElement(ElementName = "BrokerType")]
		public int BrokerType { get; set; }

		[XmlElement(ElementName = "BrokerAgentLicenseNumber")]
		public string BrokerAgentLicenseNumber { get; set; }

		[XmlElement(ElementName = "BrokerAgentCode")]
		public string BrokerAgentCode { get; set; }

		[XmlElement(ElementName = "BrokerAgentName")]
		public string BrokerAgentName { get; set; }
	}

	[XmlRoot(ElementName = "Importer")]
	public class Importer
	{

		[XmlElement(ElementName = "TaxNumber")]
		public string TaxNumber { get; set; }

		[XmlElement(ElementName = "Branch")]
		public int Branch { get; set; }

		[XmlElement(ElementName = "ThaiName")]
		public string ThaiName { get; set; }

		[XmlElement(ElementName = "EnglishName")]
		public string EnglishName { get; set; }

		[XmlElement(ElementName = "StreetAndNumber")]
		public string StreetAndNumber { get; set; }

		[XmlElement(ElementName = "District")]
		public string District { get; set; }

		[XmlElement(ElementName = "SubProvince")]
		public string SubProvince { get; set; }

		[XmlElement(ElementName = "Province")]
		public string Province { get; set; }

		[XmlElement(ElementName = "Postcode")]
		public string Postcode { get; set; }

		[XmlElement(ElementName = "PhoneNumber")]
		public string PhoneNumber { get; set; }

		[XmlElement(ElementName = "Email")]
		public string Email { get; set; }

		[XmlElement(ElementName = "BrokerInformation")]
		public BrokerInformation BrokerInformation { get; set; }
	}

	[XmlRoot(ElementName = "Agent")]
	public class Agent
	{

		[XmlElement(ElementName = "TaxNumber")]
		public string TaxNumber { get; set; }

		[XmlElement(ElementName = "Branch")]
		public int Branch { get; set; }
	}

	[XmlRoot(ElementName = "AuthorisedPerson")]
	public class AuthorisedPerson
	{

		[XmlElement(ElementName = "CustomsClearanceIDCard")]
		public string CustomsClearanceIDCard { get; set; }

		[XmlElement(ElementName = "CustomsClearanceName")]
		public string CustomsClearanceName { get; set; }

		[XmlElement(ElementName = "ManagerIDCard")]
		public string ManagerIDCard { get; set; }

		[XmlElement(ElementName = "ManagerName")]
		public string ManagerName { get; set; }
	}

	[XmlRoot(ElementName = "BorderTransportMeans")]
	public class BorderTransportMeans
	{

		[XmlElement(ElementName = "ModeCode")]
		public string ModeCode { get; set; }

		[XmlElement(ElementName = "CargoTypeCode")]
		public string CargoTypeCode { get; set; }

		[XmlElement(ElementName = "VesselName")]
		public string VesselName { get; set; }

		[XmlElement(ElementName = "ArrivalDate")]
		public string ArrivalDate { get; set; }
	}

	[XmlRoot(ElementName = "BillofLading")]
	public class BillofLading
	{

		[XmlElement(ElementName = "Master")]
		public string Master { get; set; }

		[XmlElement(ElementName = "House")]
		public string House { get; set; }
	}

	[XmlRoot(ElementName = "TotalPackage")]
	public class TotalPackage
	{

		[XmlElement(ElementName = "ShippingMarks")]
		public string ShippingMarks { get; set; }

		[XmlElement(ElementName = "Amount")]
		public int Amount { get; set; }

		[XmlElement(ElementName = "UnitCode")]
		public string UnitCode { get; set; }
	}

	[XmlRoot(ElementName = "TotalNetWeight")]
	public class TotalNetWeight
	{

		[XmlElement(ElementName = "Weight")]
		public double Weight { get; set; }

		[XmlElement(ElementName = "UnitCode")]
		public string UnitCode { get; set; }
	}

	[XmlRoot(ElementName = "TotalGrossWeight")]
	public class TotalGrossWeight
	{

		[XmlElement(ElementName = "Weight")]
		public double Weight { get; set; }

		[XmlElement(ElementName = "UnitCode")]
		public string UnitCode { get; set; }
	}

	[XmlRoot(ElementName = "SumInsured")]
	public class SumInsured
	{

		[XmlElement(ElementName = "Foreign")]
		public double Foreign { get; set; }

		[XmlElement(ElementName = "Baht")]
		public double Baht { get; set; }
	}

	[XmlRoot(ElementName = "CIFValue")]
	public class CIFValue
	{

		[XmlElement(ElementName = "Foreign")]
		public double Foreign { get; set; }

		[XmlElement(ElementName = "Baht")]
		public double Baht { get; set; }

		[XmlElement(ElementName = "Assess")]
		public double Assess { get; set; }
	}

	[XmlRoot(ElementName = "BankInfo")]
	public class BankInfo
	{

		[XmlElement(ElementName = "CustomsBankCode")]
		public int CustomsBankCode { get; set; }

		[XmlElement(ElementName = "BankCode")]
		public int BankCode { get; set; }

		[XmlElement(ElementName = "BankBranchCode")]
		public int BankBranchCode { get; set; }

		[XmlElement(ElementName = "AccountNumber")]
		public string AccountNumber { get; set; }

		[XmlElement(ElementName = "TotalPaymentAmount")]
		public double TotalPaymentAmount { get; set; }

		[XmlElement(ElementName = "PaymentMethod")]
		public string PaymentMethod { get; set; }
	}

	[XmlRoot(ElementName = "Approval")]
	public class Approval
	{

		[XmlElement(ElementName = "Port")]
		public int Port { get; set; }

		[XmlElement(ElementName = "Number")]
		public int Number { get; set; }
	}

	[XmlRoot(ElementName = "ObligationGuarantee")]
	public class ObligationGuarantee
	{

		[XmlElement(ElementName = "Method")]
		public string Method { get; set; }

		[XmlElement(ElementName = "Type")]
		public string Type { get; set; }

		[XmlElement(ElementName = "BankCode")]
		public int BankCode { get; set; }

		[XmlElement(ElementName = "BankBranchCode")]
		public int BankBranchCode { get; set; }

		[XmlElement(ElementName = "AccountNumber")]
		public string AccountNumber { get; set; }

		[XmlElement(ElementName = "TotalAmount")]
		public double TotalAmount { get; set; }
	}

	[XmlRoot(ElementName = "TradingPartner")]
	public class TradingPartner
	{

		[XmlElement(ElementName = "TaxNumber")]
		public string TaxNumber { get; set; }

		[XmlElement(ElementName = "Branch")]
		public int Branch { get; set; }
	}

	[XmlRoot(ElementName = "TaxCardInfo")]
	public class TaxCardInfo
	{

		[XmlElement(ElementName = "BankCode")]
		public int BankCode { get; set; }

		[XmlElement(ElementName = "BankBranchCode")]
		public int BankBranchCode { get; set; }

		[XmlElement(ElementName = "AccountNumber")]
		public string AccountNumber { get; set; }

		[XmlElement(ElementName = "TotalAmount")]
		public double TotalAmount { get; set; }
	}

	[XmlRoot(ElementName = "DocumentControl")]
	public class DocumentControl
	{

		//[XmlElement(ElementName = "ID")]
		//public int ID { get; set; }

		[XmlElement(ElementName = "ReferenceNumber")]
		public string ReferenceNumber { get; set; }

		[XmlElement(ElementName = "DocumentType")]
		[Required] public string DocumentType { get; set; }

		[XmlElement(ElementName = "PremiumRateType")]
		public int PremiumRateType { get; set; }

		[XmlElement(ElementName = "TypeOfClause")]
		public int TypeOfClause { get; set; }

		[XmlElement(ElementName = "Importer")]
		public Importer Importer { get; set; }

		[XmlElement(ElementName = "Agent")]
		public Agent Agent { get; set; }

		[XmlElement(ElementName = "AuthorisedPerson")]
		public AuthorisedPerson AuthorisedPerson { get; set; }

		[XmlElement(ElementName = "BorderTransportMeans")]
		public BorderTransportMeans BorderTransportMeans { get; set; }

		[XmlElement(ElementName = "BillofLading")]
		public BillofLading BillofLading { get; set; }

		[XmlElement(ElementName = "OutsideReleasePort")]
		public int OutsideReleasePort { get; set; }

		[XmlElement(ElementName = "ReleasePort")]
		public int ReleasePort { get; set; }

		[XmlElement(ElementName = "DischargePort")]
		public int DischargePort { get; set; }

		[XmlElement(ElementName = "OriginCountryCode")]
		public string OriginCountryCode { get; set; }

		[XmlElement(ElementName = "ConsignmentCountryCode")]
		public string ConsignmentCountryCode { get; set; }

		[XmlElement(ElementName = "VesselName1")]
		public string VesselName1 { get; set; }

		[XmlElement(ElementName = "VoyageNumber1")]
		public string VoyageNumber1 { get; set; }

		[XmlElement(ElementName = "VesselAge1")]
		public string VesselAge1 { get; set; }

		[XmlElement(ElementName = "VesselName2")]
		public string VesselName2 { get; set; }

		[XmlElement(ElementName = "VoyageNumber2")]
		public string VoyageNumber2 { get; set; }

		[XmlElement(ElementName = "VesselAge2")]
		public string VesselAge2 { get; set; }

		[XmlElement(ElementName = "SailingDate")]
		public string SailingDate { get; set; }

		[XmlElement(ElementName = "ConsignmentCountryDivisionsCode")]
		public string ConsignmentCountryDivisionsCode { get; set; }

		[XmlElement(ElementName = "TranshipmentCountryDivisionsCode")]
		public string TranshipmentCountryDivisionsCode { get; set; }

		[XmlElement(ElementName = "VesselType")]
		public int VesselType { get; set; }

		[XmlElement(ElementName = "VesselDetail")]
		public string VesselDetail { get; set; }

		[XmlElement(ElementName = "TotalPackage")]
		public TotalPackage TotalPackage { get; set; }

		[XmlElement(ElementName = "TotalNetWeight")]
		public TotalNetWeight TotalNetWeight { get; set; }

		[XmlElement(ElementName = "TotalGrossWeight")]
		public TotalGrossWeight TotalGrossWeight { get; set; }

		[XmlElement(ElementName = "CurrencyCode")]
		public string CurrencyCode { get; set; }

		[XmlElement(ElementName = "RateofExchange")]
		public double RateofExchange { get; set; }

		[XmlElement(ElementName = "SumInsured")]
		public SumInsured SumInsured { get; set; }

		[XmlElement(ElementName = "CIFValue")]
		public CIFValue CIFValue { get; set; }

		[XmlElement(ElementName = "RGSCode")]
		public int RGSCode { get; set; }

		[XmlElement(ElementName = "BankInfo")]
		public BankInfo BankInfo { get; set; }

		[XmlElement(ElementName = "TotalTax")]
		public double TotalTax { get; set; }

		[XmlElement(ElementName = "TotalDeposit")]
		public double TotalDeposit { get; set; }

		[XmlElement(ElementName = "CommonAccessReferenceNumber")]
		public string CommonAccessReferenceNumber { get; set; }

		[XmlElement(ElementName = "AssessmentRequestCode")]
		public string AssessmentRequestCode { get; set; }

		[XmlElement(ElementName = "InspectionRequestCode")]
		public string InspectionRequestCode { get; set; }

		[XmlElement(ElementName = "DepartureDate")]
		public string DepartureDate { get; set; }

		[XmlElement(ElementName = "Approval")]
		public Approval Approval { get; set; }

		[XmlElement(ElementName = "ObligationGuarantee")]
		public ObligationGuarantee ObligationGuarantee { get; set; }

		[XmlElement(ElementName = "ExportTaxIncentivesID")]
		public string ExportTaxIncentivesID { get; set; }

		[XmlElement(ElementName = "TradingPartner")]
		public TradingPartner TradingPartner { get; set; }

		[XmlElement(ElementName = "SubBrokerTaxNumber")]
		public string SubBrokerTaxNumber { get; set; }

		[XmlElement(ElementName = "DeferredDutyTaxFee")]
		public string DeferredDutyTaxFee { get; set; }

		[XmlElement(ElementName = "TaxCardInfo")]
		public TaxCardInfo TaxCardInfo { get; set; }

		[XmlElement(ElementName = "RegistrationID")]
		public string RegistrationID { get; set; }
	}

	[XmlRoot(ElementName = "ConsignorInfo")]
	public class ConsignorInfo
	{

		[XmlElement(ElementName = "Status")]
		public string Status { get; set; }

		[XmlElement(ElementName = "Name")]
		public string Name { get; set; }

		[XmlElement(ElementName = "StreetAndNumber")]
		public string StreetAndNumber { get; set; }

		[XmlElement(ElementName = "District")]
		public string District { get; set; }

		[XmlElement(ElementName = "SubProvince")]
		public string SubProvince { get; set; }

		[XmlElement(ElementName = "Province")]
		public string Province { get; set; }

		[XmlElement(ElementName = "Postcode")]
		public string Postcode { get; set; }

		[XmlElement(ElementName = "CountryCode")]
		public string CountryCode { get; set; }

		[XmlElement(ElementName = "Email")]
		public string Email { get; set; }
	}

	[XmlRoot(ElementName = "ForwardingCharge")]
	public class ForwardingCharge
	{

		[XmlElement(ElementName = "CurrencyCode")]
		public string CurrencyCode { get; set; }

		[XmlElement(ElementName = "Amount")]
		public double Amount { get; set; }
	}

	[XmlRoot(ElementName = "Freight")]
	public class Freight
	{

		[XmlElement(ElementName = "CurrencyCode")]
		public string CurrencyCode { get; set; }

		[XmlElement(ElementName = "Amount")]
		public double Amount { get; set; }
	}

	[XmlRoot(ElementName = "Insurance")]
	public class Insurance
	{

		[XmlElement(ElementName = "CurrencyCode")]
		public string CurrencyCode { get; set; }

		[XmlElement(ElementName = "Amount")]
		public double Amount { get; set; }
	}

	[XmlRoot(ElementName = "PackingCharge")]
	public class PackingCharge
	{

		[XmlElement(ElementName = "CurrencyCode")]
		public string CurrencyCode { get; set; }

		[XmlElement(ElementName = "Amount")]
		public double Amount { get; set; }
	}

	[XmlRoot(ElementName = "InteriorTransport")]
	public class InteriorTransport
	{

		[XmlElement(ElementName = "CurrencyCode")]
		public string CurrencyCode { get; set; }

		[XmlElement(ElementName = "Amount")]
		public double Amount { get; set; }
	}

	[XmlRoot(ElementName = "LandingCharge")]
	public class LandingCharge
	{

		[XmlElement(ElementName = "CurrencyCode")]
		public string CurrencyCode { get; set; }

		[XmlElement(ElementName = "Amount")]
		public double Amount { get; set; }
	}

	[XmlRoot(ElementName = "OtherCharge")]
	public class OtherCharge
	{

		[XmlElement(ElementName = "CurrencyCode")]
		public string CurrencyCode { get; set; }

		[XmlElement(ElementName = "Amount")]
		public double Amount { get; set; }

		[XmlElement(ElementName = "OtherChargeDescription")]
		public string OtherChargeDescription { get; set; }
	}

	[XmlRoot(ElementName = "Invoice")]
	public class Invoice
	{

		[XmlElement(ElementName = "Number")]
		public string Number { get; set; }

		[XmlElement(ElementName = "Date")]
		public string Date { get; set; }

		[XmlElement(ElementName = "PurchaseOrderNumber")]
		public string PurchaseOrderNumber { get; set; }

		[XmlElement(ElementName = "TermofPaymentCode")]
		public string TermofPaymentCode { get; set; }

		[XmlElement(ElementName = "TradeTerms")]
		public string TradeTerms { get; set; }

		[XmlElement(ElementName = "BuyerStatus")]
		public string BuyerStatus { get; set; }

		[XmlElement(ElementName = "ConsignorInfo")]
		public ConsignorInfo ConsignorInfo { get; set; }

		[XmlElement(ElementName = "CommercialLevel")]
		public string CommercialLevel { get; set; }

		[XmlElement(ElementName = "CurrencyCode")]
		public string CurrencyCode { get; set; }

		[XmlElement(ElementName = "TotalAmount")]
		public double TotalAmount { get; set; }

		[XmlElement(ElementName = "ForwardingCharge")]
		public ForwardingCharge ForwardingCharge { get; set; }

		[XmlElement(ElementName = "Freight")]
		public Freight Freight { get; set; }

		[XmlElement(ElementName = "Insurance")]
		public Insurance Insurance { get; set; }

		[XmlElement(ElementName = "PackingCharge")]
		public PackingCharge PackingCharge { get; set; }

		[XmlElement(ElementName = "InteriorTransport")]
		public InteriorTransport InteriorTransport { get; set; }

		[XmlElement(ElementName = "LandingCharge")]
		public LandingCharge LandingCharge { get; set; }

		[XmlElement(ElementName = "OtherCharge")]
		public OtherCharge OtherCharge { get; set; }

		[XmlElement(ElementName = "SelfCertificateRemark")]
		public string SelfCertificateRemark { get; set; }

		[XmlElement(ElementName = "AEOsReferenceNumber")]
		public string AEOsReferenceNumber { get; set; }

		[XmlElement(ElementName = "Item")]
		public int Item { get; set; }
	}

	[XmlRoot(ElementName = "TariffInfo")]
	public class TariffInfo
	{

		[XmlElement(ElementName = "Code")]
		public int Code { get; set; }

		[XmlElement(ElementName = "Sequence")]
		public int Sequence { get; set; }

		[XmlElement(ElementName = "Statistical")]
		public int Statistical { get; set; }

		[XmlElement(ElementName = "ImportTariff")]
		public string ImportTariff { get; set; }
	}

	[XmlRoot(ElementName = "GoodsDescription")]
	public class GoodsDescription
	{

		[XmlElement(ElementName = "Thai")]
		public string Thai { get; set; }

		[XmlElement(ElementName = "English")]
		public string English { get; set; }
	}

	[XmlRoot(ElementName = "NetWeight")]
	public class NetWeight
	{

		[XmlElement(ElementName = "Weight")]
		public double Weight { get; set; }

		[XmlElement(ElementName = "UnitCode")]
		public string UnitCode { get; set; }
	}

	[XmlRoot(ElementName = "Quantity")]
	public class Quantitys
	{

		[XmlElement(ElementName = "Quantity")]
		public double Quantity { get; set; }

		[XmlElement(ElementName = "UnitCode")]
		public string UnitCode { get; set; }
	}

	[XmlRoot(ElementName = "UnitPrice")]
	public class UnitPrice
	{

		[XmlElement(ElementName = "Foreign")]
		public double Foreign { get; set; }

		[XmlElement(ElementName = "Baht")]
		public double Baht { get; set; }
	}

	[XmlRoot(ElementName = "InvoiceQuantity")]
	public class InvoiceQuantity
	{

		[XmlElement(ElementName = "Quantity")]
		public double Quantity { get; set; }

		[XmlElement(ElementName = "UnitCode")]
		public string UnitCode { get; set; }
	}

	[XmlRoot(ElementName = "InvoiceAmount")]
	public class InvoiceAmount
	{

		[XmlElement(ElementName = "Foreign")]
		public double Foreign { get; set; }

		[XmlElement(ElementName = "Baht")]
		public double Baht { get; set; }
	}

	[XmlRoot(ElementName = "IncreasedPrice")]
	public class IncreasedPrice
	{

		[XmlElement(ElementName = "Foreign")]
		public double Foreign { get; set; }

		[XmlElement(ElementName = "Baht")]
		public double Baht { get; set; }
	}

	[XmlRoot(ElementName = "Package")]
	public class Package
	{

		[XmlElement(ElementName = "Amount")]
		public int Amount { get; set; }

		[XmlElement(ElementName = "UnitCode")]
		public string UnitCode { get; set; }
	}

	[XmlRoot(ElementName = "ProductInfo")]
	public class ProductInfo
	{

		[XmlElement(ElementName = "CustomsCode")]
		public string CustomsCode { get; set; }

		[XmlElement(ElementName = "Attribute1")]
		public string Attribute1 { get; set; }

		[XmlElement(ElementName = "Attribute2")]
		public string Attribute2 { get; set; }

		[XmlElement(ElementName = "Year")]
		public int Year { get; set; }

		[XmlElement(ElementName = "BrandName")]
		public string BrandName { get; set; }

		[XmlElement(ElementName = "Remark")]
		public string Remark { get; set; }

		[XmlElement(ElementName = "OriginCountryCode")]
		public string OriginCountryCode { get; set; }

		[XmlElement(ElementName = "NetWeight")]
		public NetWeight NetWeight { get; set; }

		[XmlElement(ElementName = "Quantity")]
		public Quantitys Quantity { get; set; }

		[XmlElement(ElementName = "CurrencyCode")]
		public string CurrencyCode { get; set; }

		[XmlElement(ElementName = "ExchangeRate")]
		public double ExchangeRate { get; set; }

		[XmlElement(ElementName = "UnitPrice")]
		public UnitPrice UnitPrice { get; set; }

		[XmlElement(ElementName = "InvoiceQuantity")]
		public InvoiceQuantity InvoiceQuantity { get; set; }

		[XmlElement(ElementName = "InvoiceAmount")]
		public InvoiceAmount InvoiceAmount { get; set; }

		[XmlElement(ElementName = "IncreasedPrice")]
		public IncreasedPrice IncreasedPrice { get; set; }

		[XmlElement(ElementName = "CIFValue")]
		public CIFValue CIFValue { get; set; }

		[XmlElement(ElementName = "ShippingMarks")]
		public string ShippingMarks { get; set; }

		[XmlElement(ElementName = "Package")]
		public Package Package { get; set; }
	}

	[XmlRoot(ElementName = "ReferenceDeclaration")]
	public class ReferenceDeclaration
	{

		[XmlElement(ElementName = "Number")]
		public string Number { get; set; }

		[XmlElement(ElementName = "LineNumber")]
		public int LineNumber { get; set; }
	}

	[XmlRoot(ElementName = "ExciseQuantity")]
	public class ExciseQuantity
	{

		[XmlElement(ElementName = "Quantity")]
		public double Quantity { get; set; }

		[XmlElement(ElementName = "UnitCode")]
		public string UnitCode { get; set; }

		[XmlElement(ElementName = "AssessQuantity")]
		public double AssessQuantity { get; set; }
	}

	[XmlRoot(ElementName = "ArgumentativeTariff")]
	public class ArgumentativeTariff
	{

		[XmlElement(ElementName = "Code")]
		public int Code { get; set; }

		[XmlElement(ElementName = "Sequence")]
		public int Sequence { get; set; }

		[XmlElement(ElementName = "PrivilegeCode")]
		public string PrivilegeCode { get; set; }

		[XmlElement(ElementName = "ReasonCode")]
		public string ReasonCode { get; set; }
	}

	[XmlRoot(ElementName = "ModelInfo")]
	public class ModelInfo
	{

		[XmlElement(ElementName = "Number")]
		public string Number { get; set; }

		[XmlElement(ElementName = "Version")]
		public int Version { get; set; }

		[XmlElement(ElementName = "CompanyTaxNumber")]
		public string CompanyTaxNumber { get; set; }
	}

	[XmlRoot(ElementName = "Rate")]
	public class Rate
	{

		[XmlElement(ElementName = "Value")]
		public double Value { get; set; }

		[XmlElement(ElementName = "Specific")]
		public double Specific { get; set; }

		[XmlElement(ElementName = "SpecificUnitCode")]
		public string SpecificUnitCode { get; set; }

		[XmlElement(ElementName = "Exemption")]
		public double Exemption { get; set; }
	}

	[XmlRoot(ElementName = "Amount")]
	public class Amounts
	{

		[XmlElement(ElementName = "Amount")]
		public double Amount { get; set; }

		[XmlElement(ElementName = "Paid")]
		public double Paid { get; set; }

		[XmlElement(ElementName = "Deposit")]
		public double Deposit { get; set; }
	}

	[XmlRoot(ElementName = "Argumentative")]
	public class Argumentative
	{

		[XmlElement(ElementName = "ValueRate")]
		public double ValueRate { get; set; }

		[XmlElement(ElementName = "SpecificRate")]
		public double SpecificRate { get; set; }

		[XmlElement(ElementName = "SpecificUnitCode")]
		public string SpecificUnitCode { get; set; }
	}

	[XmlRoot(ElementName = "Deposit")]
	public class Deposit
	{

		[XmlElement(ElementName = "ReasonCode")]
		public string ReasonCode { get; set; }

		[XmlElement(ElementName = "Amount")]
		public double Amount { get; set; }
	}

	[XmlRoot(ElementName = "Duty")]
	public class Duty
	{

		[XmlElement(ElementName = "Type")]
		public string Type { get; set; }

		[XmlElement(ElementName = "Rate")]
		public Rate Rate { get; set; }

		[XmlElement(ElementName = "Amount")]
		public Amounts Amount { get; set; }

		[XmlElement(ElementName = "InterestRate")]
		public double InterestRate { get; set; }

		[XmlElement(ElementName = "Month")]
		public int Month { get; set; }

		[XmlElement(ElementName = "Interest")]
		public double Interest { get; set; }

		[XmlElement(ElementName = "Argumentative")]
		public Argumentative Argumentative { get; set; }

		[XmlElement(ElementName = "Deposit")]
		public Deposit Deposit { get; set; }
	}

	[XmlRoot(ElementName = "IssueInfo")]
	public class IssueInfo
	{

		[XmlElement(ElementName = "Date")]
		public string Date { get; set; }

		[XmlElement(ElementName = "Authority")]
		public string Authority { get; set; }
	}

	[XmlRoot(ElementName = "Permit")]
	public class Permit
	{

		[XmlElement(ElementName = "Number")]
		public string Number { get; set; }

		[XmlElement(ElementName = "IssueInfo")]
		public IssueInfo IssueInfo { get; set; }
	}

	[XmlRoot(ElementName = "Detail")]
	public class Detail
	{

		[XmlElement(ElementName = "ItemNumber")]
		public int ItemNumber { get; set; }

		[XmlElement(ElementName = "Invoice")]
		public Invoice Invoice { get; set; }

		[XmlElement(ElementName = "TariffInfo")]
		public TariffInfo TariffInfo { get; set; }

		[XmlElement(ElementName = "PrivilegeCode")]
		public string PrivilegeCode { get; set; }

		[XmlElement(ElementName = "AHTNCode")]
		public int AHTNCode { get; set; }

		[XmlElement(ElementName = "NatureofTransaction")]
		public int NatureofTransaction { get; set; }

		[XmlElement(ElementName = "UNDGNumber")]
		public int UNDGNumber { get; set; }

		[XmlElement(ElementName = "ProductCode")]
		public string ProductCode { get; set; }

		[XmlElement(ElementName = "GoodsDescription")]
		public GoodsDescription GoodsDescription { get; set; }

		[XmlElement(ElementName = "ProductInfo")]
		public ProductInfo ProductInfo { get; set; }

		[XmlElement(ElementName = "ReImportationCertificate")]
		public string ReImportationCertificate { get; set; }

		[XmlElement(ElementName = "BOI")]
		public string BOI { get; set; }

		[XmlElement(ElementName = "Bond")]
		public string Bond { get; set; }

		[XmlElement(ElementName = "Bis19")]
		public string Bis19 { get; set; }

		[XmlElement(ElementName = "ReExport")]
		public string ReExport { get; set; }

		[XmlElement(ElementName = "FZ")]
		public string FZ { get; set; }

		[XmlElement(ElementName = "IEAT")]
		public string IEAT { get; set; }

		[XmlElement(ElementName = "Serveral")]
		public string Serveral { get; set; }

		[XmlElement(ElementName = "ReferenceDeclaration")]
		public ReferenceDeclaration ReferenceDeclaration { get; set; }

		[XmlElement(ElementName = "AssessQuantity")]
		public double AssessQuantity { get; set; }

		[XmlElement(ElementName = "ExemptCIFBaht")]
		public double ExemptCIFBaht { get; set; }

		[XmlElement(ElementName = "ExciseCode")]
		public int ExciseCode { get; set; }

		[XmlElement(ElementName = "ExciseQuantity")]
		public ExciseQuantity ExciseQuantity { get; set; }

		[XmlElement(ElementName = "ImportTaxIncentivesID")]
		public string ImportTaxIncentivesID { get; set; }

		[XmlElement(ElementName = "ArgumentativeTariff")]
		public ArgumentativeTariff ArgumentativeTariff { get; set; }

		[XmlElement(ElementName = "BOILicenseNumber")]
		public string BOILicenseNumber { get; set; }

		[XmlElement(ElementName = "OriginCriteria")]
		public string OriginCriteria { get; set; }

		[XmlElement(ElementName = "CertifiedExporterNumber")]
		public string CertifiedExporterNumber { get; set; }

		[XmlElement(ElementName = "GrossWeight")]
		public double GrossWeight { get; set; }

		[XmlElement(ElementName = "ProcedureCode")]
		public string ProcedureCode { get; set; }

		[XmlElement(ElementName = "ValuationCode")]
		public int ValuationCode { get; set; }

		[XmlElement(ElementName = "DeductedAmount")]
		public double DeductedAmount { get; set; }

		[XmlElement(ElementName = "ModelInfo")]
		public ModelInfo ModelInfo { get; set; }

		[XmlElement(ElementName = "Royalty")]
		public string Royalty { get; set; }

		[XmlElement(ElementName = "RoyaltyDetail")]
		public string RoyaltyDetail { get; set; }

		[XmlElement(ElementName = "LastEntry")]
		public string LastEntry { get; set; }

		[XmlElement(ElementName = "Duty")]
		public Duty Duty { get; set; }

		[XmlElement(ElementName = "Permit")]
		public Permit Permit { get; set; }
	}

	[XmlRoot(ElementName = "GoodsShipment")]
	public class GoodsShipment
	{

		[XmlElement(ElementName = "Invoice")]
		public Invoice Invoice { get; set; }

		[XmlElement(ElementName = "Detail")]
		public Detail Detail { get; set; }
	}
	
	[XmlRoot(ElementName = "CustomsImportDeclaration",Namespace = "http://itrade.tradesiam.com/XMLSchema/Import_Policy_Request_and_Confirm_Message_V.1.0")]
	public class CustomsImportDeclaration
	{

		[XmlElement(ElementName = "DocumentControl")]
		public DocumentControl DocumentControl { get; set; }

		[XmlElement(ElementName = "GoodsShipment")]
		public GoodsShipment GoodsShipment { get; set; }

		
	}


}