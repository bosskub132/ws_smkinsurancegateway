﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;
//using System.Xml.Serialization;

//namespace WS_smkinsurancegateway.Models
//{
//    //[XmlRoot(ElementName = "REQUEST")]
//    //[XmlElement(ElementName = "SearchNo")]
//    //namespace PolicyValidate_request2
//    //{
//    //    [XmlRoot(ElementName = "REQUEST")]
//    //    public class REQUEST
//    //    {
//    //        [XmlElement(ElementName = "LogId")]
//    //        public string LogId { get; set; }

//    //        [XmlElement(DataType ="String",ElementName = "SearchNo")]
//    //        public string SearchNo { get; set; }
//    //    }
//    //}

//    // NOTE: Generated code may require at least .NET Framework 4.5 or .NET Core/Standard 2.0.
//    /// <remarks/>
//    [System.SerializableAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
//    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
//    public partial class CustomsImportDeclaration
//    {

//        private CustomsImportDeclarationDocumentControl documentControlField;

//        private CustomsImportDeclarationGoodsShipment goodsShipmentField;

//        /// <remarks/>
//        public CustomsImportDeclarationDocumentControl DocumentControl
//        {
//            get
//            {
//                return this.documentControlField;
//            }
//            set
//            {
//                this.documentControlField = value;
//            }
//        }

//        /// <remarks/>
//        public CustomsImportDeclarationGoodsShipment GoodsShipment
//        {
//            get
//            {
//                return this.goodsShipmentField;
//            }
//            set
//            {
//                this.goodsShipmentField = value;
//            }
//        }
//    }

//    /// <remarks/>
//    [System.SerializableAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
//    public partial class CustomsImportDeclarationDocumentControl
//    {

//        private object idField;

//        private string referenceNumberField;

//        private string documentTypeField;

//        private object premiumRateTypeField;

//        private object typeOfClauseField;

//        private CustomsImportDeclarationDocumentControlImporter importerField;

//        private CustomsImportDeclarationDocumentControlAgent agentField;

//        private CustomsImportDeclarationDocumentControlAuthorisedPerson authorisedPersonField;

//        private CustomsImportDeclarationDocumentControlBorderTransportMeans borderTransportMeansField;

//        private CustomsImportDeclarationDocumentControlBillofLading billofLadingField;

//        private object outsideReleasePortField;

//        private object releasePortField;

//        private object dischargePortField;

//        private string originCountryCodeField;

//        private string consignmentCountryCodeField;

//        private string vesselName1Field;

//        private string voyageNumber1Field;

//        private string vesselAge1Field;

//        private string vesselName2Field;

//        private string voyageNumber2Field;

//        private string vesselAge2Field;

//        private object sailingDateField;

//        private string consignmentCountryDivisionsCodeField;

//        private string transhipmentCountryDivisionsCodeField;

//        private object vesselTypeField;

//        private string vesselDetailField;

//        private CustomsImportDeclarationDocumentControlTotalPackage totalPackageField;

//        private CustomsImportDeclarationDocumentControlTotalNetWeight totalNetWeightField;

//        private CustomsImportDeclarationDocumentControlTotalGrossWeight totalGrossWeightField;

//        private string currencyCodeField;

//        private object rateofExchangeField;

//        private CustomsImportDeclarationDocumentControlSumInsured sumInsuredField;

//        private CustomsImportDeclarationDocumentControlCIFValue cIFValueField;

//        private object rGSCodeField;

//        private CustomsImportDeclarationDocumentControlBankInfo bankInfoField;

//        private object totalTaxField;

//        private object totalDepositField;

//        private string commonAccessReferenceNumberField;

//        private string assessmentRequestCodeField;

//        private string inspectionRequestCodeField;

//        private object departureDateField;

//        private CustomsImportDeclarationDocumentControlApproval approvalField;

//        private CustomsImportDeclarationDocumentControlObligationGuarantee obligationGuaranteeField;

//        private string exportTaxIncentivesIDField;

//        private CustomsImportDeclarationDocumentControlTradingPartner tradingPartnerField;

//        private string subBrokerTaxNumberField;

//        private string deferredDutyTaxFeeField;

//        private CustomsImportDeclarationDocumentControlTaxCardInfo taxCardInfoField;

//        private string registrationIDField;

//        /// <remarks/>
//        public object ID
//        {
//            get
//            {
//                return this.idField;
//            }
//            set
//            {
//                this.idField = value;
//            }
//        }

//        /// <remarks/>
//        public string ReferenceNumber
//        {
//            get
//            {
//                return this.referenceNumberField;
//            }
//            set
//            {
//                this.referenceNumberField = value;
//            }
//        }

//        /// <remarks/>
//        public string DocumentType
//        {
//            get
//            {
//                return this.documentTypeField;
//            }
//            set
//            {
//                this.documentTypeField = value;
//            }
//        }

//        /// <remarks/>
//        public object PremiumRateType
//        {
//            get
//            {
//                return this.premiumRateTypeField;
//            }
//            set
//            {
//                this.premiumRateTypeField = value;
//            }
//        }

//        /// <remarks/>
//        public object TypeOfClause
//        {
//            get
//            {
//                return this.typeOfClauseField;
//            }
//            set
//            {
//                this.typeOfClauseField = value;
//            }
//        }

//        /// <remarks/>
//        public CustomsImportDeclarationDocumentControlImporter Importer
//        {
//            get
//            {
//                return this.importerField;
//            }
//            set
//            {
//                this.importerField = value;
//            }
//        }

//        /// <remarks/>
//        public CustomsImportDeclarationDocumentControlAgent Agent
//        {
//            get
//            {
//                return this.agentField;
//            }
//            set
//            {
//                this.agentField = value;
//            }
//        }

//        /// <remarks/>
//        public CustomsImportDeclarationDocumentControlAuthorisedPerson AuthorisedPerson
//        {
//            get
//            {
//                return this.authorisedPersonField;
//            }
//            set
//            {
//                this.authorisedPersonField = value;
//            }
//        }

//        /// <remarks/>
//        public CustomsImportDeclarationDocumentControlBorderTransportMeans BorderTransportMeans
//        {
//            get
//            {
//                return this.borderTransportMeansField;
//            }
//            set
//            {
//                this.borderTransportMeansField = value;
//            }
//        }

//        /// <remarks/>
//        public CustomsImportDeclarationDocumentControlBillofLading BillofLading
//        {
//            get
//            {
//                return this.billofLadingField;
//            }
//            set
//            {
//                this.billofLadingField = value;
//            }
//        }

//        /// <remarks/>
//        public object OutsideReleasePort
//        {
//            get
//            {
//                return this.outsideReleasePortField;
//            }
//            set
//            {
//                this.outsideReleasePortField = value;
//            }
//        }

//        /// <remarks/>
//        public object ReleasePort
//        {
//            get
//            {
//                return this.releasePortField;
//            }
//            set
//            {
//                this.releasePortField = value;
//            }
//        }

//        /// <remarks/>
//        public object DischargePort
//        {
//            get
//            {
//                return this.dischargePortField;
//            }
//            set
//            {
//                this.dischargePortField = value;
//            }
//        }

//        /// <remarks/>
//        public string OriginCountryCode
//        {
//            get
//            {
//                return this.originCountryCodeField;
//            }
//            set
//            {
//                this.originCountryCodeField = value;
//            }
//        }

//        /// <remarks/>
//        public string ConsignmentCountryCode
//        {
//            get
//            {
//                return this.consignmentCountryCodeField;
//            }
//            set
//            {
//                this.consignmentCountryCodeField = value;
//            }
//        }

//        /// <remarks/>
//        public string VesselName1
//        {
//            get
//            {
//                return this.vesselName1Field;
//            }
//            set
//            {
//                this.vesselName1Field = value;
//            }
//        }

//        /// <remarks/>
//        public string VoyageNumber1
//        {
//            get
//            {
//                return this.voyageNumber1Field;
//            }
//            set
//            {
//                this.voyageNumber1Field = value;
//            }
//        }

//        /// <remarks/>
//        public string VesselAge1
//        {
//            get
//            {
//                return this.vesselAge1Field;
//            }
//            set
//            {
//                this.vesselAge1Field = value;
//            }
//        }

//        /// <remarks/>
//        public string VesselName2
//        {
//            get
//            {
//                return this.vesselName2Field;
//            }
//            set
//            {
//                this.vesselName2Field = value;
//            }
//        }

//        /// <remarks/>
//        public string VoyageNumber2
//        {
//            get
//            {
//                return this.voyageNumber2Field;
//            }
//            set
//            {
//                this.voyageNumber2Field = value;
//            }
//        }

//        /// <remarks/>
//        public string VesselAge2
//        {
//            get
//            {
//                return this.vesselAge2Field;
//            }
//            set
//            {
//                this.vesselAge2Field = value;
//            }
//        }

//        /// <remarks/>
//        public object SailingDate
//        {
//            get
//            {
//                return this.sailingDateField;
//            }
//            set
//            {
//                this.sailingDateField = value;
//            }
//        }

//        /// <remarks/>
//        public string ConsignmentCountryDivisionsCode
//        {
//            get
//            {
//                return this.consignmentCountryDivisionsCodeField;
//            }
//            set
//            {
//                this.consignmentCountryDivisionsCodeField = value;
//            }
//        }

//        /// <remarks/>
//        public string TranshipmentCountryDivisionsCode
//        {
//            get
//            {
//                return this.transhipmentCountryDivisionsCodeField;
//            }
//            set
//            {
//                this.transhipmentCountryDivisionsCodeField = value;
//            }
//        }

//        /// <remarks/>
//        public object VesselType
//        {
//            get
//            {
//                return this.vesselTypeField;
//            }
//            set
//            {
//                this.vesselTypeField = value;
//            }
//        }

//        /// <remarks/>
//        public string VesselDetail
//        {
//            get
//            {
//                return this.vesselDetailField;
//            }
//            set
//            {
//                this.vesselDetailField = value;
//            }
//        }

//        /// <remarks/>
//        public CustomsImportDeclarationDocumentControlTotalPackage TotalPackage
//        {
//            get
//            {
//                return this.totalPackageField;
//            }
//            set
//            {
//                this.totalPackageField = value;
//            }
//        }

//        /// <remarks/>
//        public CustomsImportDeclarationDocumentControlTotalNetWeight TotalNetWeight
//        {
//            get
//            {
//                return this.totalNetWeightField;
//            }
//            set
//            {
//                this.totalNetWeightField = value;
//            }
//        }

//        /// <remarks/>
//        public CustomsImportDeclarationDocumentControlTotalGrossWeight TotalGrossWeight
//        {
//            get
//            {
//                return this.totalGrossWeightField;
//            }
//            set
//            {
//                this.totalGrossWeightField = value;
//            }
//        }

//        /// <remarks/>
//        public string CurrencyCode
//        {
//            get
//            {
//                return this.currencyCodeField;
//            }
//            set
//            {
//                this.currencyCodeField = value;
//            }
//        }

//        /// <remarks/>
//        public object RateofExchange
//        {
//            get
//            {
//                return this.rateofExchangeField;
//            }
//            set
//            {
//                this.rateofExchangeField = value;
//            }
//        }

//        /// <remarks/>
//        public CustomsImportDeclarationDocumentControlSumInsured SumInsured
//        {
//            get
//            {
//                return this.sumInsuredField;
//            }
//            set
//            {
//                this.sumInsuredField = value;
//            }
//        }

//        /// <remarks/>
//        public CustomsImportDeclarationDocumentControlCIFValue CIFValue
//        {
//            get
//            {
//                return this.cIFValueField;
//            }
//            set
//            {
//                this.cIFValueField = value;
//            }
//        }

//        /// <remarks/>
//        public object RGSCode
//        {
//            get
//            {
//                return this.rGSCodeField;
//            }
//            set
//            {
//                this.rGSCodeField = value;
//            }
//        }

//        /// <remarks/>
//        public CustomsImportDeclarationDocumentControlBankInfo BankInfo
//        {
//            get
//            {
//                return this.bankInfoField;
//            }
//            set
//            {
//                this.bankInfoField = value;
//            }
//        }

//        /// <remarks/>
//        public object TotalTax
//        {
//            get
//            {
//                return this.totalTaxField;
//            }
//            set
//            {
//                this.totalTaxField = value;
//            }
//        }

//        /// <remarks/>
//        public object TotalDeposit
//        {
//            get
//            {
//                return this.totalDepositField;
//            }
//            set
//            {
//                this.totalDepositField = value;
//            }
//        }

//        /// <remarks/>
//        public string CommonAccessReferenceNumber
//        {
//            get
//            {
//                return this.commonAccessReferenceNumberField;
//            }
//            set
//            {
//                this.commonAccessReferenceNumberField = value;
//            }
//        }

//        /// <remarks/>
//        public string AssessmentRequestCode
//        {
//            get
//            {
//                return this.assessmentRequestCodeField;
//            }
//            set
//            {
//                this.assessmentRequestCodeField = value;
//            }
//        }

//        /// <remarks/>
//        public string InspectionRequestCode
//        {
//            get
//            {
//                return this.inspectionRequestCodeField;
//            }
//            set
//            {
//                this.inspectionRequestCodeField = value;
//            }
//        }

//        /// <remarks/>
//        public object DepartureDate
//        {
//            get
//            {
//                return this.departureDateField;
//            }
//            set
//            {
//                this.departureDateField = value;
//            }
//        }

//        /// <remarks/>
//        public CustomsImportDeclarationDocumentControlApproval Approval
//        {
//            get
//            {
//                return this.approvalField;
//            }
//            set
//            {
//                this.approvalField = value;
//            }
//        }

//        /// <remarks/>
//        public CustomsImportDeclarationDocumentControlObligationGuarantee ObligationGuarantee
//        {
//            get
//            {
//                return this.obligationGuaranteeField;
//            }
//            set
//            {
//                this.obligationGuaranteeField = value;
//            }
//        }

//        /// <remarks/>
//        public string ExportTaxIncentivesID
//        {
//            get
//            {
//                return this.exportTaxIncentivesIDField;
//            }
//            set
//            {
//                this.exportTaxIncentivesIDField = value;
//            }
//        }

//        /// <remarks/>
//        public CustomsImportDeclarationDocumentControlTradingPartner TradingPartner
//        {
//            get
//            {
//                return this.tradingPartnerField;
//            }
//            set
//            {
//                this.tradingPartnerField = value;
//            }
//        }

//        /// <remarks/>
//        public string SubBrokerTaxNumber
//        {
//            get
//            {
//                return this.subBrokerTaxNumberField;
//            }
//            set
//            {
//                this.subBrokerTaxNumberField = value;
//            }
//        }

//        /// <remarks/>
//        public string DeferredDutyTaxFee
//        {
//            get
//            {
//                return this.deferredDutyTaxFeeField;
//            }
//            set
//            {
//                this.deferredDutyTaxFeeField = value;
//            }
//        }

//        /// <remarks/>
//        public CustomsImportDeclarationDocumentControlTaxCardInfo TaxCardInfo
//        {
//            get
//            {
//                return this.taxCardInfoField;
//            }
//            set
//            {
//                this.taxCardInfoField = value;
//            }
//        }

//        /// <remarks/>
//        public string RegistrationID
//        {
//            get
//            {
//                return this.registrationIDField;
//            }
//            set
//            {
//                this.registrationIDField = value;
//            }
//        }
//    }

//    /// <remarks/>
//    [System.SerializableAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
//    public partial class CustomsImportDeclarationDocumentControlImporter
//    {

//        private string taxNumberField;

//        private object branchField;

//        private string thaiNameField;

//        private string englishNameField;

//        private string streetAndNumberField;

//        private string districtField;

//        private string subProvinceField;

//        private string provinceField;

//        private string postcodeField;

//        private string phoneNumberField;

//        private string emailField;

//        private CustomsImportDeclarationDocumentControlImporterBrokerInformation brokerInformationField;

//        /// <remarks/>
//        public string TaxNumber
//        {
//            get
//            {
//                return this.taxNumberField;
//            }
//            set
//            {
//                this.taxNumberField = value;
//            }
//        }

//        /// <remarks/>
//        public object Branch
//        {
//            get
//            {
//                return this.branchField;
//            }
//            set
//            {
//                this.branchField = value;
//            }
//        }

//        /// <remarks/>
//        public string ThaiName
//        {
//            get
//            {
//                return this.thaiNameField;
//            }
//            set
//            {
//                this.thaiNameField = value;
//            }
//        }

//        /// <remarks/>
//        public string EnglishName
//        {
//            get
//            {
//                return this.englishNameField;
//            }
//            set
//            {
//                this.englishNameField = value;
//            }
//        }

//        /// <remarks/>
//        public string StreetAndNumber
//        {
//            get
//            {
//                return this.streetAndNumberField;
//            }
//            set
//            {
//                this.streetAndNumberField = value;
//            }
//        }

//        /// <remarks/>
//        public string District
//        {
//            get
//            {
//                return this.districtField;
//            }
//            set
//            {
//                this.districtField = value;
//            }
//        }

//        /// <remarks/>
//        public string SubProvince
//        {
//            get
//            {
//                return this.subProvinceField;
//            }
//            set
//            {
//                this.subProvinceField = value;
//            }
//        }

//        /// <remarks/>
//        public string Province
//        {
//            get
//            {
//                return this.provinceField;
//            }
//            set
//            {
//                this.provinceField = value;
//            }
//        }

//        /// <remarks/>
//        public string Postcode
//        {
//            get
//            {
//                return this.postcodeField;
//            }
//            set
//            {
//                this.postcodeField = value;
//            }
//        }

//        /// <remarks/>
//        public string PhoneNumber
//        {
//            get
//            {
//                return this.phoneNumberField;
//            }
//            set
//            {
//                this.phoneNumberField = value;
//            }
//        }

//        /// <remarks/>
//        public string Email
//        {
//            get
//            {
//                return this.emailField;
//            }
//            set
//            {
//                this.emailField = value;
//            }
//        }

//        /// <remarks/>
//        public CustomsImportDeclarationDocumentControlImporterBrokerInformation BrokerInformation
//        {
//            get
//            {
//                return this.brokerInformationField;
//            }
//            set
//            {
//                this.brokerInformationField = value;
//            }
//        }
//    }

//    /// <remarks/>
//    [System.SerializableAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
//    public partial class CustomsImportDeclarationDocumentControlImporterBrokerInformation
//    {

//        private object brokerTypeField;

//        private string brokerAgentLicenseNumberField;

//        private string brokerAgentCodeField;

//        private string brokerAgentNameField;

//        /// <remarks/>
//        public object BrokerType
//        {
//            get
//            {
//                return this.brokerTypeField;
//            }
//            set
//            {
//                this.brokerTypeField = value;
//            }
//        }

//        /// <remarks/>
//        public string BrokerAgentLicenseNumber
//        {
//            get
//            {
//                return this.brokerAgentLicenseNumberField;
//            }
//            set
//            {
//                this.brokerAgentLicenseNumberField = value;
//            }
//        }

//        /// <remarks/>
//        public string BrokerAgentCode
//        {
//            get
//            {
//                return this.brokerAgentCodeField;
//            }
//            set
//            {
//                this.brokerAgentCodeField = value;
//            }
//        }

//        /// <remarks/>
//        public string BrokerAgentName
//        {
//            get
//            {
//                return this.brokerAgentNameField;
//            }
//            set
//            {
//                this.brokerAgentNameField = value;
//            }
//        }
//    }

//    /// <remarks/>
//    [System.SerializableAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
//    public partial class CustomsImportDeclarationDocumentControlAgent
//    {

//        private string taxNumberField;

//        private object branchField;

//        /// <remarks/>
//        public string TaxNumber
//        {
//            get
//            {
//                return this.taxNumberField;
//            }
//            set
//            {
//                this.taxNumberField = value;
//            }
//        }

//        /// <remarks/>
//        public object Branch
//        {
//            get
//            {
//                return this.branchField;
//            }
//            set
//            {
//                this.branchField = value;
//            }
//        }
//    }

//    /// <remarks/>
//    [System.SerializableAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
//    public partial class CustomsImportDeclarationDocumentControlAuthorisedPerson
//    {

//        private string customsClearanceIDCardField;

//        private string customsClearanceNameField;

//        private string managerIDCardField;

//        private string managerNameField;

//        /// <remarks/>
//        public string CustomsClearanceIDCard
//        {
//            get
//            {
//                return this.customsClearanceIDCardField;
//            }
//            set
//            {
//                this.customsClearanceIDCardField = value;
//            }
//        }

//        /// <remarks/>
//        public string CustomsClearanceName
//        {
//            get
//            {
//                return this.customsClearanceNameField;
//            }
//            set
//            {
//                this.customsClearanceNameField = value;
//            }
//        }

//        /// <remarks/>
//        public string ManagerIDCard
//        {
//            get
//            {
//                return this.managerIDCardField;
//            }
//            set
//            {
//                this.managerIDCardField = value;
//            }
//        }

//        /// <remarks/>
//        public string ManagerName
//        {
//            get
//            {
//                return this.managerNameField;
//            }
//            set
//            {
//                this.managerNameField = value;
//            }
//        }
//    }

//    /// <remarks/>
//    [System.SerializableAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
//    public partial class CustomsImportDeclarationDocumentControlBorderTransportMeans
//    {

//        private string modeCodeField;

//        private string cargoTypeCodeField;

//        private string vesselNameField;

//        private object arrivalDateField;

//        /// <remarks/>
//        public string ModeCode
//        {
//            get
//            {
//                return this.modeCodeField;
//            }
//            set
//            {
//                this.modeCodeField = value;
//            }
//        }

//        /// <remarks/>
//        public string CargoTypeCode
//        {
//            get
//            {
//                return this.cargoTypeCodeField;
//            }
//            set
//            {
//                this.cargoTypeCodeField = value;
//            }
//        }

//        /// <remarks/>
//        public string VesselName
//        {
//            get
//            {
//                return this.vesselNameField;
//            }
//            set
//            {
//                this.vesselNameField = value;
//            }
//        }

//        /// <remarks/>
//        public object ArrivalDate
//        {
//            get
//            {
//                return this.arrivalDateField;
//            }
//            set
//            {
//                this.arrivalDateField = value;
//            }
//        }
//    }

//    /// <remarks/>
//    [System.SerializableAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
//    public partial class CustomsImportDeclarationDocumentControlBillofLading
//    {

//        private string masterField;

//        private string houseField;

//        /// <remarks/>
//        public string Master
//        {
//            get
//            {
//                return this.masterField;
//            }
//            set
//            {
//                this.masterField = value;
//            }
//        }

//        /// <remarks/>
//        public string House
//        {
//            get
//            {
//                return this.houseField;
//            }
//            set
//            {
//                this.houseField = value;
//            }
//        }
//    }

//    /// <remarks/>
//    [System.SerializableAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
//    public partial class CustomsImportDeclarationDocumentControlTotalPackage
//    {

//        private string shippingMarksField;

//        private object amountField;

//        private string unitCodeField;

//        /// <remarks/>
//        public string ShippingMarks
//        {
//            get
//            {
//                return this.shippingMarksField;
//            }
//            set
//            {
//                this.shippingMarksField = value;
//            }
//        }

//        /// <remarks/>
//        public object Amount
//        {
//            get
//            {
//                return this.amountField;
//            }
//            set
//            {
//                this.amountField = value;
//            }
//        }

//        /// <remarks/>
//        public string UnitCode
//        {
//            get
//            {
//                return this.unitCodeField;
//            }
//            set
//            {
//                this.unitCodeField = value;
//            }
//        }
//    }

//    /// <remarks/>
//    [System.SerializableAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
//    public partial class CustomsImportDeclarationDocumentControlTotalNetWeight
//    {

//        private object weightField;

//        private string unitCodeField;

//        /// <remarks/>
//        public object Weight
//        {
//            get
//            {
//                return this.weightField;
//            }
//            set
//            {
//                this.weightField = value;
//            }
//        }

//        /// <remarks/>
//        public string UnitCode
//        {
//            get
//            {
//                return this.unitCodeField;
//            }
//            set
//            {
//                this.unitCodeField = value;
//            }
//        }
//    }

//    /// <remarks/>
//    [System.SerializableAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
//    public partial class CustomsImportDeclarationDocumentControlTotalGrossWeight
//    {

//        private object weightField;

//        private string unitCodeField;

//        /// <remarks/>
//        public object Weight
//        {
//            get
//            {
//                return this.weightField;
//            }
//            set
//            {
//                this.weightField = value;
//            }
//        }

//        /// <remarks/>
//        public string UnitCode
//        {
//            get
//            {
//                return this.unitCodeField;
//            }
//            set
//            {
//                this.unitCodeField = value;
//            }
//        }
//    }

//    /// <remarks/>
//    [System.SerializableAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
//    public partial class CustomsImportDeclarationDocumentControlSumInsured
//    {

//        private object foreignField;

//        private object bahtField;

//        /// <remarks/>
//        public object Foreign
//        {
//            get
//            {
//                return this.foreignField;
//            }
//            set
//            {
//                this.foreignField = value;
//            }
//        }

//        /// <remarks/>
//        public object Baht
//        {
//            get
//            {
//                return this.bahtField;
//            }
//            set
//            {
//                this.bahtField = value;
//            }
//        }
//    }

//    /// <remarks/>
//    [System.SerializableAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
//    public partial class CustomsImportDeclarationDocumentControlCIFValue
//    {

//        private object foreignField;

//        private object bahtField;

//        /// <remarks/>
//        public object Foreign
//        {
//            get
//            {
//                return this.foreignField;
//            }
//            set
//            {
//                this.foreignField = value;
//            }
//        }

//        /// <remarks/>
//        public object Baht
//        {
//            get
//            {
//                return this.bahtField;
//            }
//            set
//            {
//                this.bahtField = value;
//            }
//        }
//    }

//    /// <remarks/>
//    [System.SerializableAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
//    public partial class CustomsImportDeclarationDocumentControlBankInfo
//    {

//        private object customsBankCodeField;

//        private object bankCodeField;

//        private object bankBranchCodeField;

//        private string accountNumberField;

//        private object totalPaymentAmountField;

//        private string paymentMethodField;

//        /// <remarks/>
//        public object CustomsBankCode
//        {
//            get
//            {
//                return this.customsBankCodeField;
//            }
//            set
//            {
//                this.customsBankCodeField = value;
//            }
//        }

//        /// <remarks/>
//        public object BankCode
//        {
//            get
//            {
//                return this.bankCodeField;
//            }
//            set
//            {
//                this.bankCodeField = value;
//            }
//        }

//        /// <remarks/>
//        public object BankBranchCode
//        {
//            get
//            {
//                return this.bankBranchCodeField;
//            }
//            set
//            {
//                this.bankBranchCodeField = value;
//            }
//        }

//        /// <remarks/>
//        public string AccountNumber
//        {
//            get
//            {
//                return this.accountNumberField;
//            }
//            set
//            {
//                this.accountNumberField = value;
//            }
//        }

//        /// <remarks/>
//        public object TotalPaymentAmount
//        {
//            get
//            {
//                return this.totalPaymentAmountField;
//            }
//            set
//            {
//                this.totalPaymentAmountField = value;
//            }
//        }

//        /// <remarks/>
//        public string PaymentMethod
//        {
//            get
//            {
//                return this.paymentMethodField;
//            }
//            set
//            {
//                this.paymentMethodField = value;
//            }
//        }
//    }

//    /// <remarks/>
//    [System.SerializableAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
//    public partial class CustomsImportDeclarationDocumentControlApproval
//    {

//        private object portField;

//        private object numberField;

//        /// <remarks/>
//        public object Port
//        {
//            get
//            {
//                return this.portField;
//            }
//            set
//            {
//                this.portField = value;
//            }
//        }

//        /// <remarks/>
//        public object Number
//        {
//            get
//            {
//                return this.numberField;
//            }
//            set
//            {
//                this.numberField = value;
//            }
//        }
//    }

//    /// <remarks/>
//    [System.SerializableAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
//    public partial class CustomsImportDeclarationDocumentControlObligationGuarantee
//    {

//        private string methodField;

//        private string typeField;

//        private object bankCodeField;

//        private object bankBranchCodeField;

//        private string accountNumberField;

//        private object totalAmountField;

//        /// <remarks/>
//        public string Method
//        {
//            get
//            {
//                return this.methodField;
//            }
//            set
//            {
//                this.methodField = value;
//            }
//        }

//        /// <remarks/>
//        public string Type
//        {
//            get
//            {
//                return this.typeField;
//            }
//            set
//            {
//                this.typeField = value;
//            }
//        }

//        /// <remarks/>
//        public object BankCode
//        {
//            get
//            {
//                return this.bankCodeField;
//            }
//            set
//            {
//                this.bankCodeField = value;
//            }
//        }

//        /// <remarks/>
//        public object BankBranchCode
//        {
//            get
//            {
//                return this.bankBranchCodeField;
//            }
//            set
//            {
//                this.bankBranchCodeField = value;
//            }
//        }

//        /// <remarks/>
//        public string AccountNumber
//        {
//            get
//            {
//                return this.accountNumberField;
//            }
//            set
//            {
//                this.accountNumberField = value;
//            }
//        }

//        /// <remarks/>
//        public object TotalAmount
//        {
//            get
//            {
//                return this.totalAmountField;
//            }
//            set
//            {
//                this.totalAmountField = value;
//            }
//        }
//    }

//    /// <remarks/>
//    [System.SerializableAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
//    public partial class CustomsImportDeclarationDocumentControlTradingPartner
//    {

//        private string taxNumberField;

//        private object branchField;

//        /// <remarks/>
//        public string TaxNumber
//        {
//            get
//            {
//                return this.taxNumberField;
//            }
//            set
//            {
//                this.taxNumberField = value;
//            }
//        }

//        /// <remarks/>
//        public object Branch
//        {
//            get
//            {
//                return this.branchField;
//            }
//            set
//            {
//                this.branchField = value;
//            }
//        }
//    }

//    /// <remarks/>
//    [System.SerializableAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
//    public partial class CustomsImportDeclarationDocumentControlTaxCardInfo
//    {

//        private object bankCodeField;

//        private object bankBranchCodeField;

//        private string accountNumberField;

//        private object totalAmountField;

//        /// <remarks/>
//        public object BankCode
//        {
//            get
//            {
//                return this.bankCodeField;
//            }
//            set
//            {
//                this.bankCodeField = value;
//            }
//        }

//        /// <remarks/>
//        public object BankBranchCode
//        {
//            get
//            {
//                return this.bankBranchCodeField;
//            }
//            set
//            {
//                this.bankBranchCodeField = value;
//            }
//        }

//        /// <remarks/>
//        public string AccountNumber
//        {
//            get
//            {
//                return this.accountNumberField;
//            }
//            set
//            {
//                this.accountNumberField = value;
//            }
//        }

//        /// <remarks/>
//        public object TotalAmount
//        {
//            get
//            {
//                return this.totalAmountField;
//            }
//            set
//            {
//                this.totalAmountField = value;
//            }
//        }
//    }

//    /// <remarks/>
//    [System.SerializableAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
//    public partial class CustomsImportDeclarationGoodsShipment
//    {

//        private CustomsImportDeclarationGoodsShipmentInvoice invoiceField;

//        private CustomsImportDeclarationGoodsShipmentDetail detailField;

//        /// <remarks/>
//        public CustomsImportDeclarationGoodsShipmentInvoice Invoice
//        {
//            get
//            {
//                return this.invoiceField;
//            }
//            set
//            {
//                this.invoiceField = value;
//            }
//        }

//        /// <remarks/>
//        public CustomsImportDeclarationGoodsShipmentDetail Detail
//        {
//            get
//            {
//                return this.detailField;
//            }
//            set
//            {
//                this.detailField = value;
//            }
//        }
//    }

//    /// <remarks/>
//    [System.SerializableAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
//    public partial class CustomsImportDeclarationGoodsShipmentInvoice
//    {

//        private string numberField;

//        private object dateField;

//        private string purchaseOrderNumberField;

//        private string termofPaymentCodeField;

//        private string tradeTermsField;

//        private string buyerStatusField;

//        private CustomsImportDeclarationGoodsShipmentInvoiceConsignorInfo consignorInfoField;

//        private string commercialLevelField;

//        private string currencyCodeField;

//        private object totalAmountField;

//        private CustomsImportDeclarationGoodsShipmentInvoiceForwardingCharge forwardingChargeField;

//        private CustomsImportDeclarationGoodsShipmentInvoiceFreight freightField;

//        private CustomsImportDeclarationGoodsShipmentInvoiceInsurance insuranceField;

//        private CustomsImportDeclarationGoodsShipmentInvoicePackingCharge packingChargeField;

//        private CustomsImportDeclarationGoodsShipmentInvoiceInteriorTransport interiorTransportField;

//        private CustomsImportDeclarationGoodsShipmentInvoiceLandingCharge landingChargeField;

//        private CustomsImportDeclarationGoodsShipmentInvoiceOtherCharge otherChargeField;

//        private string selfCertificateRemarkField;

//        private string aEOsReferenceNumberField;

//        /// <remarks/>
//        public string Number
//        {
//            get
//            {
//                return this.numberField;
//            }
//            set
//            {
//                this.numberField = value;
//            }
//        }

//        /// <remarks/>
//        public object Date
//        {
//            get
//            {
//                return this.dateField;
//            }
//            set
//            {
//                this.dateField = value;
//            }
//        }

//        /// <remarks/>
//        public string PurchaseOrderNumber
//        {
//            get
//            {
//                return this.purchaseOrderNumberField;
//            }
//            set
//            {
//                this.purchaseOrderNumberField = value;
//            }
//        }

//        /// <remarks/>
//        public string TermofPaymentCode
//        {
//            get
//            {
//                return this.termofPaymentCodeField;
//            }
//            set
//            {
//                this.termofPaymentCodeField = value;
//            }
//        }

//        /// <remarks/>
//        public string TradeTerms
//        {
//            get
//            {
//                return this.tradeTermsField;
//            }
//            set
//            {
//                this.tradeTermsField = value;
//            }
//        }

//        /// <remarks/>
//        public string BuyerStatus
//        {
//            get
//            {
//                return this.buyerStatusField;
//            }
//            set
//            {
//                this.buyerStatusField = value;
//            }
//        }

//        /// <remarks/>
//        public CustomsImportDeclarationGoodsShipmentInvoiceConsignorInfo ConsignorInfo
//        {
//            get
//            {
//                return this.consignorInfoField;
//            }
//            set
//            {
//                this.consignorInfoField = value;
//            }
//        }

//        /// <remarks/>
//        public string CommercialLevel
//        {
//            get
//            {
//                return this.commercialLevelField;
//            }
//            set
//            {
//                this.commercialLevelField = value;
//            }
//        }

//        /// <remarks/>
//        public string CurrencyCode
//        {
//            get
//            {
//                return this.currencyCodeField;
//            }
//            set
//            {
//                this.currencyCodeField = value;
//            }
//        }

//        /// <remarks/>
//        public object TotalAmount
//        {
//            get
//            {
//                return this.totalAmountField;
//            }
//            set
//            {
//                this.totalAmountField = value;
//            }
//        }

//        /// <remarks/>
//        public CustomsImportDeclarationGoodsShipmentInvoiceForwardingCharge ForwardingCharge
//        {
//            get
//            {
//                return this.forwardingChargeField;
//            }
//            set
//            {
//                this.forwardingChargeField = value;
//            }
//        }

//        /// <remarks/>
//        public CustomsImportDeclarationGoodsShipmentInvoiceFreight Freight
//        {
//            get
//            {
//                return this.freightField;
//            }
//            set
//            {
//                this.freightField = value;
//            }
//        }

//        /// <remarks/>
//        public CustomsImportDeclarationGoodsShipmentInvoiceInsurance Insurance
//        {
//            get
//            {
//                return this.insuranceField;
//            }
//            set
//            {
//                this.insuranceField = value;
//            }
//        }

//        /// <remarks/>
//        public CustomsImportDeclarationGoodsShipmentInvoicePackingCharge PackingCharge
//        {
//            get
//            {
//                return this.packingChargeField;
//            }
//            set
//            {
//                this.packingChargeField = value;
//            }
//        }

//        /// <remarks/>
//        public CustomsImportDeclarationGoodsShipmentInvoiceInteriorTransport InteriorTransport
//        {
//            get
//            {
//                return this.interiorTransportField;
//            }
//            set
//            {
//                this.interiorTransportField = value;
//            }
//        }

//        /// <remarks/>
//        public CustomsImportDeclarationGoodsShipmentInvoiceLandingCharge LandingCharge
//        {
//            get
//            {
//                return this.landingChargeField;
//            }
//            set
//            {
//                this.landingChargeField = value;
//            }
//        }

//        /// <remarks/>
//        public CustomsImportDeclarationGoodsShipmentInvoiceOtherCharge OtherCharge
//        {
//            get
//            {
//                return this.otherChargeField;
//            }
//            set
//            {
//                this.otherChargeField = value;
//            }
//        }

//        /// <remarks/>
//        public string SelfCertificateRemark
//        {
//            get
//            {
//                return this.selfCertificateRemarkField;
//            }
//            set
//            {
//                this.selfCertificateRemarkField = value;
//            }
//        }

//        /// <remarks/>
//        public string AEOsReferenceNumber
//        {
//            get
//            {
//                return this.aEOsReferenceNumberField;
//            }
//            set
//            {
//                this.aEOsReferenceNumberField = value;
//            }
//        }
//    }

//    /// <remarks/>
//    [System.SerializableAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
//    public partial class CustomsImportDeclarationGoodsShipmentInvoiceConsignorInfo
//    {

//        private string statusField;

//        private string nameField;

//        private string streetAndNumberField;

//        private string districtField;

//        private string subProvinceField;

//        private string provinceField;

//        private string postcodeField;

//        private string countryCodeField;

//        private string emailField;

//        /// <remarks/>
//        public string Status
//        {
//            get
//            {
//                return this.statusField;
//            }
//            set
//            {
//                this.statusField = value;
//            }
//        }

//        /// <remarks/>
//        public string Name
//        {
//            get
//            {
//                return this.nameField;
//            }
//            set
//            {
//                this.nameField = value;
//            }
//        }

//        /// <remarks/>
//        public string StreetAndNumber
//        {
//            get
//            {
//                return this.streetAndNumberField;
//            }
//            set
//            {
//                this.streetAndNumberField = value;
//            }
//        }

//        /// <remarks/>
//        public string District
//        {
//            get
//            {
//                return this.districtField;
//            }
//            set
//            {
//                this.districtField = value;
//            }
//        }

//        /// <remarks/>
//        public string SubProvince
//        {
//            get
//            {
//                return this.subProvinceField;
//            }
//            set
//            {
//                this.subProvinceField = value;
//            }
//        }

//        /// <remarks/>
//        public string Province
//        {
//            get
//            {
//                return this.provinceField;
//            }
//            set
//            {
//                this.provinceField = value;
//            }
//        }

//        /// <remarks/>
//        public string Postcode
//        {
//            get
//            {
//                return this.postcodeField;
//            }
//            set
//            {
//                this.postcodeField = value;
//            }
//        }

//        /// <remarks/>
//        public string CountryCode
//        {
//            get
//            {
//                return this.countryCodeField;
//            }
//            set
//            {
//                this.countryCodeField = value;
//            }
//        }

//        /// <remarks/>
//        public string Email
//        {
//            get
//            {
//                return this.emailField;
//            }
//            set
//            {
//                this.emailField = value;
//            }
//        }
//    }

//    /// <remarks/>
//    [System.SerializableAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
//    public partial class CustomsImportDeclarationGoodsShipmentInvoiceForwardingCharge
//    {

//        private string currencyCodeField;

//        private object amountField;

//        /// <remarks/>
//        public string CurrencyCode
//        {
//            get
//            {
//                return this.currencyCodeField;
//            }
//            set
//            {
//                this.currencyCodeField = value;
//            }
//        }

//        /// <remarks/>
//        public object Amount
//        {
//            get
//            {
//                return this.amountField;
//            }
//            set
//            {
//                this.amountField = value;
//            }
//        }
//    }

//    /// <remarks/>
//    [System.SerializableAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
//    public partial class CustomsImportDeclarationGoodsShipmentInvoiceFreight
//    {

//        private string currencyCodeField;

//        private object amountField;

//        /// <remarks/>
//        public string CurrencyCode
//        {
//            get
//            {
//                return this.currencyCodeField;
//            }
//            set
//            {
//                this.currencyCodeField = value;
//            }
//        }

//        /// <remarks/>
//        public object Amount
//        {
//            get
//            {
//                return this.amountField;
//            }
//            set
//            {
//                this.amountField = value;
//            }
//        }
//    }

//    /// <remarks/>
//    [System.SerializableAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
//    public partial class CustomsImportDeclarationGoodsShipmentInvoiceInsurance
//    {

//        private string currencyCodeField;

//        private object amountField;

//        /// <remarks/>
//        public string CurrencyCode
//        {
//            get
//            {
//                return this.currencyCodeField;
//            }
//            set
//            {
//                this.currencyCodeField = value;
//            }
//        }

//        /// <remarks/>
//        public object Amount
//        {
//            get
//            {
//                return this.amountField;
//            }
//            set
//            {
//                this.amountField = value;
//            }
//        }
//    }

//    /// <remarks/>
//    [System.SerializableAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
//    public partial class CustomsImportDeclarationGoodsShipmentInvoicePackingCharge
//    {

//        private string currencyCodeField;

//        private object amountField;

//        /// <remarks/>
//        public string CurrencyCode
//        {
//            get
//            {
//                return this.currencyCodeField;
//            }
//            set
//            {
//                this.currencyCodeField = value;
//            }
//        }

//        /// <remarks/>
//        public object Amount
//        {
//            get
//            {
//                return this.amountField;
//            }
//            set
//            {
//                this.amountField = value;
//            }
//        }
//    }

//    /// <remarks/>
//    [System.SerializableAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
//    public partial class CustomsImportDeclarationGoodsShipmentInvoiceInteriorTransport
//    {

//        private string currencyCodeField;

//        private object amountField;

//        /// <remarks/>
//        public string CurrencyCode
//        {
//            get
//            {
//                return this.currencyCodeField;
//            }
//            set
//            {
//                this.currencyCodeField = value;
//            }
//        }

//        /// <remarks/>
//        public object Amount
//        {
//            get
//            {
//                return this.amountField;
//            }
//            set
//            {
//                this.amountField = value;
//            }
//        }
//    }

//    /// <remarks/>
//    [System.SerializableAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
//    public partial class CustomsImportDeclarationGoodsShipmentInvoiceLandingCharge
//    {

//        private string currencyCodeField;

//        private object amountField;

//        /// <remarks/>
//        public string CurrencyCode
//        {
//            get
//            {
//                return this.currencyCodeField;
//            }
//            set
//            {
//                this.currencyCodeField = value;
//            }
//        }

//        /// <remarks/>
//        public object Amount
//        {
//            get
//            {
//                return this.amountField;
//            }
//            set
//            {
//                this.amountField = value;
//            }
//        }
//    }

//    /// <remarks/>
//    [System.SerializableAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
//    public partial class CustomsImportDeclarationGoodsShipmentInvoiceOtherCharge
//    {

//        private string currencyCodeField;

//        private object amountField;

//        private string otherChargeDescriptionField;

//        /// <remarks/>
//        public string CurrencyCode
//        {
//            get
//            {
//                return this.currencyCodeField;
//            }
//            set
//            {
//                this.currencyCodeField = value;
//            }
//        }

//        /// <remarks/>
//        public object Amount
//        {
//            get
//            {
//                return this.amountField;
//            }
//            set
//            {
//                this.amountField = value;
//            }
//        }

//        /// <remarks/>
//        public string OtherChargeDescription
//        {
//            get
//            {
//                return this.otherChargeDescriptionField;
//            }
//            set
//            {
//                this.otherChargeDescriptionField = value;
//            }
//        }
//    }

//    /// <remarks/>
//    [System.SerializableAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
//    public partial class CustomsImportDeclarationGoodsShipmentDetail
//    {

//        private object itemNumberField;

//        private CustomsImportDeclarationGoodsShipmentDetailInvoice invoiceField;

//        private CustomsImportDeclarationGoodsShipmentDetailTariffInfo tariffInfoField;

//        private string privilegeCodeField;

//        private object aHTNCodeField;

//        private object natureofTransactionField;

//        private object uNDGNumberField;

//        private string productCodeField;

//        private CustomsImportDeclarationGoodsShipmentDetailGoodsDescription goodsDescriptionField;

//        private CustomsImportDeclarationGoodsShipmentDetailProductInfo productInfoField;

//        private object reImportationCertificateField;

//        private object bOIField;

//        private object bondField;

//        private object bis19Field;

//        private object reExportField;

//        private object fzField;

//        private object iEATField;

//        private object serveralField;

//        private CustomsImportDeclarationGoodsShipmentDetailReferenceDeclaration referenceDeclarationField;

//        private object assessQuantityField;

//        private object exemptCIFBahtField;

//        private object exciseCodeField;

//        private CustomsImportDeclarationGoodsShipmentDetailExciseQuantity exciseQuantityField;

//        private string importTaxIncentivesIDField;

//        private CustomsImportDeclarationGoodsShipmentDetailArgumentativeTariff argumentativeTariffField;

//        private string bOILicenseNumberField;

//        private string originCriteriaField;

//        private string certifiedExporterNumberField;

//        private object grossWeightField;

//        private string procedureCodeField;

//        private ushort valuationCodeField;

//        private object deductedAmountField;

//        private CustomsImportDeclarationGoodsShipmentDetailModelInfo modelInfoField;

//        private string royaltyField;

//        private string royaltyDetailField;

//        private object lastEntryField;

//        private CustomsImportDeclarationGoodsShipmentDetailDuty dutyField;

//        private CustomsImportDeclarationGoodsShipmentDetailPermit permitField;

//        /// <remarks/>
//        public object ItemNumber
//        {
//            get
//            {
//                return this.itemNumberField;
//            }
//            set
//            {
//                this.itemNumberField = value;
//            }
//        }

//        /// <remarks/>
//        public CustomsImportDeclarationGoodsShipmentDetailInvoice Invoice
//        {
//            get
//            {
//                return this.invoiceField;
//            }
//            set
//            {
//                this.invoiceField = value;
//            }
//        }

//        /// <remarks/>
//        public CustomsImportDeclarationGoodsShipmentDetailTariffInfo TariffInfo
//        {
//            get
//            {
//                return this.tariffInfoField;
//            }
//            set
//            {
//                this.tariffInfoField = value;
//            }
//        }

//        /// <remarks/>
//        public string PrivilegeCode
//        {
//            get
//            {
//                return this.privilegeCodeField;
//            }
//            set
//            {
//                this.privilegeCodeField = value;
//            }
//        }

//        /// <remarks/>
//        public object AHTNCode
//        {
//            get
//            {
//                return this.aHTNCodeField;
//            }
//            set
//            {
//                this.aHTNCodeField = value;
//            }
//        }

//        /// <remarks/>
//        public object NatureofTransaction
//        {
//            get
//            {
//                return this.natureofTransactionField;
//            }
//            set
//            {
//                this.natureofTransactionField = value;
//            }
//        }

//        /// <remarks/>
//        public object UNDGNumber
//        {
//            get
//            {
//                return this.uNDGNumberField;
//            }
//            set
//            {
//                this.uNDGNumberField = value;
//            }
//        }

//        /// <remarks/>
//        public string ProductCode
//        {
//            get
//            {
//                return this.productCodeField;
//            }
//            set
//            {
//                this.productCodeField = value;
//            }
//        }

//        /// <remarks/>
//        public CustomsImportDeclarationGoodsShipmentDetailGoodsDescription GoodsDescription
//        {
//            get
//            {
//                return this.goodsDescriptionField;
//            }
//            set
//            {
//                this.goodsDescriptionField = value;
//            }
//        }

//        /// <remarks/>
//        public CustomsImportDeclarationGoodsShipmentDetailProductInfo ProductInfo
//        {
//            get
//            {
//                return this.productInfoField;
//            }
//            set
//            {
//                this.productInfoField = value;
//            }
//        }

//        /// <remarks/>
//        public object ReImportationCertificate
//        {
//            get
//            {
//                return this.reImportationCertificateField;
//            }
//            set
//            {
//                this.reImportationCertificateField = value;
//            }
//        }

//        /// <remarks/>
//        public object BOI
//        {
//            get
//            {
//                return this.bOIField;
//            }
//            set
//            {
//                this.bOIField = value;
//            }
//        }

//        /// <remarks/>
//        public object Bond
//        {
//            get
//            {
//                return this.bondField;
//            }
//            set
//            {
//                this.bondField = value;
//            }
//        }

//        /// <remarks/>
//        public object Bis19
//        {
//            get
//            {
//                return this.bis19Field;
//            }
//            set
//            {
//                this.bis19Field = value;
//            }
//        }

//        /// <remarks/>
//        public object ReExport
//        {
//            get
//            {
//                return this.reExportField;
//            }
//            set
//            {
//                this.reExportField = value;
//            }
//        }

//        /// <remarks/>
//        public object FZ
//        {
//            get
//            {
//                return this.fzField;
//            }
//            set
//            {
//                this.fzField = value;
//            }
//        }

//        /// <remarks/>
//        public object IEAT
//        {
//            get
//            {
//                return this.iEATField;
//            }
//            set
//            {
//                this.iEATField = value;
//            }
//        }

//        /// <remarks/>
//        public object Serveral
//        {
//            get
//            {
//                return this.serveralField;
//            }
//            set
//            {
//                this.serveralField = value;
//            }
//        }

//        /// <remarks/>
//        public CustomsImportDeclarationGoodsShipmentDetailReferenceDeclaration ReferenceDeclaration
//        {
//            get
//            {
//                return this.referenceDeclarationField;
//            }
//            set
//            {
//                this.referenceDeclarationField = value;
//            }
//        }

//        /// <remarks/>
//        public object AssessQuantity
//        {
//            get
//            {
//                return this.assessQuantityField;
//            }
//            set
//            {
//                this.assessQuantityField = value;
//            }
//        }

//        /// <remarks/>
//        public object ExemptCIFBaht
//        {
//            get
//            {
//                return this.exemptCIFBahtField;
//            }
//            set
//            {
//                this.exemptCIFBahtField = value;
//            }
//        }

//        /// <remarks/>
//        public object ExciseCode
//        {
//            get
//            {
//                return this.exciseCodeField;
//            }
//            set
//            {
//                this.exciseCodeField = value;
//            }
//        }

//        /// <remarks/>
//        public CustomsImportDeclarationGoodsShipmentDetailExciseQuantity ExciseQuantity
//        {
//            get
//            {
//                return this.exciseQuantityField;
//            }
//            set
//            {
//                this.exciseQuantityField = value;
//            }
//        }

//        /// <remarks/>
//        public string ImportTaxIncentivesID
//        {
//            get
//            {
//                return this.importTaxIncentivesIDField;
//            }
//            set
//            {
//                this.importTaxIncentivesIDField = value;
//            }
//        }

//        /// <remarks/>
//        public CustomsImportDeclarationGoodsShipmentDetailArgumentativeTariff ArgumentativeTariff
//        {
//            get
//            {
//                return this.argumentativeTariffField;
//            }
//            set
//            {
//                this.argumentativeTariffField = value;
//            }
//        }

//        /// <remarks/>
//        public string BOILicenseNumber
//        {
//            get
//            {
//                return this.bOILicenseNumberField;
//            }
//            set
//            {
//                this.bOILicenseNumberField = value;
//            }
//        }

//        /// <remarks/>
//        public string OriginCriteria
//        {
//            get
//            {
//                return this.originCriteriaField;
//            }
//            set
//            {
//                this.originCriteriaField = value;
//            }
//        }

//        /// <remarks/>
//        public string CertifiedExporterNumber
//        {
//            get
//            {
//                return this.certifiedExporterNumberField;
//            }
//            set
//            {
//                this.certifiedExporterNumberField = value;
//            }
//        }

//        /// <remarks/>
//        public object GrossWeight
//        {
//            get
//            {
//                return this.grossWeightField;
//            }
//            set
//            {
//                this.grossWeightField = value;
//            }
//        }

//        /// <remarks/>
//        public string ProcedureCode
//        {
//            get
//            {
//                return this.procedureCodeField;
//            }
//            set
//            {
//                this.procedureCodeField = value;
//            }
//        }

//        /// <remarks/>
//        public ushort ValuationCode
//        {
//            get
//            {
//                return this.valuationCodeField;
//            }
//            set
//            {
//                this.valuationCodeField = value;
//            }
//        }

//        /// <remarks/>
//        public object DeductedAmount
//        {
//            get
//            {
//                return this.deductedAmountField;
//            }
//            set
//            {
//                this.deductedAmountField = value;
//            }
//        }

//        /// <remarks/>
//        public CustomsImportDeclarationGoodsShipmentDetailModelInfo ModelInfo
//        {
//            get
//            {
//                return this.modelInfoField;
//            }
//            set
//            {
//                this.modelInfoField = value;
//            }
//        }

//        /// <remarks/>
//        public string Royalty
//        {
//            get
//            {
//                return this.royaltyField;
//            }
//            set
//            {
//                this.royaltyField = value;
//            }
//        }

//        /// <remarks/>
//        public string RoyaltyDetail
//        {
//            get
//            {
//                return this.royaltyDetailField;
//            }
//            set
//            {
//                this.royaltyDetailField = value;
//            }
//        }

//        /// <remarks/>
//        public object LastEntry
//        {
//            get
//            {
//                return this.lastEntryField;
//            }
//            set
//            {
//                this.lastEntryField = value;
//            }
//        }

//        /// <remarks/>
//        public CustomsImportDeclarationGoodsShipmentDetailDuty Duty
//        {
//            get
//            {
//                return this.dutyField;
//            }
//            set
//            {
//                this.dutyField = value;
//            }
//        }

//        /// <remarks/>
//        public CustomsImportDeclarationGoodsShipmentDetailPermit Permit
//        {
//            get
//            {
//                return this.permitField;
//            }
//            set
//            {
//                this.permitField = value;
//            }
//        }
//    }

//    /// <remarks/>
//    [System.SerializableAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
//    public partial class CustomsImportDeclarationGoodsShipmentDetailInvoice
//    {

//        private string numberField;

//        private object itemField;

//        /// <remarks/>
//        public string Number
//        {
//            get
//            {
//                return this.numberField;
//            }
//            set
//            {
//                this.numberField = value;
//            }
//        }

//        /// <remarks/>
//        public object Item
//        {
//            get
//            {
//                return this.itemField;
//            }
//            set
//            {
//                this.itemField = value;
//            }
//        }
//    }

//    /// <remarks/>
//    [System.SerializableAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
//    public partial class CustomsImportDeclarationGoodsShipmentDetailTariffInfo
//    {

//        private object codeField;

//        private object sequenceField;

//        private object statisticalField;

//        private string importTariffField;

//        /// <remarks/>
//        public object Code
//        {
//            get
//            {
//                return this.codeField;
//            }
//            set
//            {
//                this.codeField = value;
//            }
//        }

//        /// <remarks/>
//        public object Sequence
//        {
//            get
//            {
//                return this.sequenceField;
//            }
//            set
//            {
//                this.sequenceField = value;
//            }
//        }

//        /// <remarks/>
//        public object Statistical
//        {
//            get
//            {
//                return this.statisticalField;
//            }
//            set
//            {
//                this.statisticalField = value;
//            }
//        }

//        /// <remarks/>
//        public string ImportTariff
//        {
//            get
//            {
//                return this.importTariffField;
//            }
//            set
//            {
//                this.importTariffField = value;
//            }
//        }
//    }

//    /// <remarks/>
//    [System.SerializableAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
//    public partial class CustomsImportDeclarationGoodsShipmentDetailGoodsDescription
//    {

//        private string thaiField;

//        private string englishField;

//        /// <remarks/>
//        public string Thai
//        {
//            get
//            {
//                return this.thaiField;
//            }
//            set
//            {
//                this.thaiField = value;
//            }
//        }

//        /// <remarks/>
//        public string English
//        {
//            get
//            {
//                return this.englishField;
//            }
//            set
//            {
//                this.englishField = value;
//            }
//        }
//    }

//    /// <remarks/>
//    [System.SerializableAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
//    public partial class CustomsImportDeclarationGoodsShipmentDetailProductInfo
//    {

//        private string customsCodeField;

//        private string attribute1Field;

//        private string attribute2Field;

//        private object yearField;

//        private string brandNameField;

//        private string remarkField;

//        private string originCountryCodeField;

//        private CustomsImportDeclarationGoodsShipmentDetailProductInfoNetWeight netWeightField;

//        private CustomsImportDeclarationGoodsShipmentDetailProductInfoQuantity quantityField;

//        private string currencyCodeField;

//        private object exchangeRateField;

//        private CustomsImportDeclarationGoodsShipmentDetailProductInfoUnitPrice unitPriceField;

//        private CustomsImportDeclarationGoodsShipmentDetailProductInfoInvoiceQuantity invoiceQuantityField;

//        private CustomsImportDeclarationGoodsShipmentDetailProductInfoInvoiceAmount invoiceAmountField;

//        private CustomsImportDeclarationGoodsShipmentDetailProductInfoIncreasedPrice increasedPriceField;

//        private CustomsImportDeclarationGoodsShipmentDetailProductInfoCIFValue cIFValueField;

//        private string shippingMarksField;

//        private CustomsImportDeclarationGoodsShipmentDetailProductInfoPackage packageField;

//        /// <remarks/>
//        public string CustomsCode
//        {
//            get
//            {
//                return this.customsCodeField;
//            }
//            set
//            {
//                this.customsCodeField = value;
//            }
//        }

//        /// <remarks/>
//        public string Attribute1
//        {
//            get
//            {
//                return this.attribute1Field;
//            }
//            set
//            {
//                this.attribute1Field = value;
//            }
//        }

//        /// <remarks/>
//        public string Attribute2
//        {
//            get
//            {
//                return this.attribute2Field;
//            }
//            set
//            {
//                this.attribute2Field = value;
//            }
//        }

//        /// <remarks/>
//        public object Year
//        {
//            get
//            {
//                return this.yearField;
//            }
//            set
//            {
//                this.yearField = value;
//            }
//        }

//        /// <remarks/>
//        public string BrandName
//        {
//            get
//            {
//                return this.brandNameField;
//            }
//            set
//            {
//                this.brandNameField = value;
//            }
//        }

//        /// <remarks/>
//        public string Remark
//        {
//            get
//            {
//                return this.remarkField;
//            }
//            set
//            {
//                this.remarkField = value;
//            }
//        }

//        /// <remarks/>
//        public string OriginCountryCode
//        {
//            get
//            {
//                return this.originCountryCodeField;
//            }
//            set
//            {
//                this.originCountryCodeField = value;
//            }
//        }

//        /// <remarks/>
//        public CustomsImportDeclarationGoodsShipmentDetailProductInfoNetWeight NetWeight
//        {
//            get
//            {
//                return this.netWeightField;
//            }
//            set
//            {
//                this.netWeightField = value;
//            }
//        }

//        /// <remarks/>
//        public CustomsImportDeclarationGoodsShipmentDetailProductInfoQuantity Quantity
//        {
//            get
//            {
//                return this.quantityField;
//            }
//            set
//            {
//                this.quantityField = value;
//            }
//        }

//        /// <remarks/>
//        public string CurrencyCode
//        {
//            get
//            {
//                return this.currencyCodeField;
//            }
//            set
//            {
//                this.currencyCodeField = value;
//            }
//        }

//        /// <remarks/>
//        public object ExchangeRate
//        {
//            get
//            {
//                return this.exchangeRateField;
//            }
//            set
//            {
//                this.exchangeRateField = value;
//            }
//        }

//        /// <remarks/>
//        public CustomsImportDeclarationGoodsShipmentDetailProductInfoUnitPrice UnitPrice
//        {
//            get
//            {
//                return this.unitPriceField;
//            }
//            set
//            {
//                this.unitPriceField = value;
//            }
//        }

//        /// <remarks/>
//        public CustomsImportDeclarationGoodsShipmentDetailProductInfoInvoiceQuantity InvoiceQuantity
//        {
//            get
//            {
//                return this.invoiceQuantityField;
//            }
//            set
//            {
//                this.invoiceQuantityField = value;
//            }
//        }

//        /// <remarks/>
//        public CustomsImportDeclarationGoodsShipmentDetailProductInfoInvoiceAmount InvoiceAmount
//        {
//            get
//            {
//                return this.invoiceAmountField;
//            }
//            set
//            {
//                this.invoiceAmountField = value;
//            }
//        }

//        /// <remarks/>
//        public CustomsImportDeclarationGoodsShipmentDetailProductInfoIncreasedPrice IncreasedPrice
//        {
//            get
//            {
//                return this.increasedPriceField;
//            }
//            set
//            {
//                this.increasedPriceField = value;
//            }
//        }

//        /// <remarks/>
//        public CustomsImportDeclarationGoodsShipmentDetailProductInfoCIFValue CIFValue
//        {
//            get
//            {
//                return this.cIFValueField;
//            }
//            set
//            {
//                this.cIFValueField = value;
//            }
//        }

//        /// <remarks/>
//        public string ShippingMarks
//        {
//            get
//            {
//                return this.shippingMarksField;
//            }
//            set
//            {
//                this.shippingMarksField = value;
//            }
//        }

//        /// <remarks/>
//        public CustomsImportDeclarationGoodsShipmentDetailProductInfoPackage Package
//        {
//            get
//            {
//                return this.packageField;
//            }
//            set
//            {
//                this.packageField = value;
//            }
//        }
//    }

//    /// <remarks/>
//    [System.SerializableAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
//    public partial class CustomsImportDeclarationGoodsShipmentDetailProductInfoNetWeight
//    {

//        private object weightField;

//        private string unitCodeField;

//        /// <remarks/>
//        public object Weight
//        {
//            get
//            {
//                return this.weightField;
//            }
//            set
//            {
//                this.weightField = value;
//            }
//        }

//        /// <remarks/>
//        public string UnitCode
//        {
//            get
//            {
//                return this.unitCodeField;
//            }
//            set
//            {
//                this.unitCodeField = value;
//            }
//        }
//    }

//    /// <remarks/>
//    [System.SerializableAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
//    public partial class CustomsImportDeclarationGoodsShipmentDetailProductInfoQuantity
//    {

//        private object quantityField;

//        private string unitCodeField;

//        /// <remarks/>
//        public object Quantity
//        {
//            get
//            {
//                return this.quantityField;
//            }
//            set
//            {
//                this.quantityField = value;
//            }
//        }

//        /// <remarks/>
//        public string UnitCode
//        {
//            get
//            {
//                return this.unitCodeField;
//            }
//            set
//            {
//                this.unitCodeField = value;
//            }
//        }
//    }

//    /// <remarks/>
//    [System.SerializableAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
//    public partial class CustomsImportDeclarationGoodsShipmentDetailProductInfoUnitPrice
//    {

//        private object foreignField;

//        private object bahtField;

//        /// <remarks/>
//        public object Foreign
//        {
//            get
//            {
//                return this.foreignField;
//            }
//            set
//            {
//                this.foreignField = value;
//            }
//        }

//        /// <remarks/>
//        public object Baht
//        {
//            get
//            {
//                return this.bahtField;
//            }
//            set
//            {
//                this.bahtField = value;
//            }
//        }
//    }

//    /// <remarks/>
//    [System.SerializableAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
//    public partial class CustomsImportDeclarationGoodsShipmentDetailProductInfoInvoiceQuantity
//    {

//        private object quantityField;

//        private string unitCodeField;

//        /// <remarks/>
//        public object Quantity
//        {
//            get
//            {
//                return this.quantityField;
//            }
//            set
//            {
//                this.quantityField = value;
//            }
//        }

//        /// <remarks/>
//        public string UnitCode
//        {
//            get
//            {
//                return this.unitCodeField;
//            }
//            set
//            {
//                this.unitCodeField = value;
//            }
//        }
//    }

//    /// <remarks/>
//    [System.SerializableAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
//    public partial class CustomsImportDeclarationGoodsShipmentDetailProductInfoInvoiceAmount
//    {

//        private object foreignField;

//        private object bahtField;

//        /// <remarks/>
//        public object Foreign
//        {
//            get
//            {
//                return this.foreignField;
//            }
//            set
//            {
//                this.foreignField = value;
//            }
//        }

//        /// <remarks/>
//        public object Baht
//        {
//            get
//            {
//                return this.bahtField;
//            }
//            set
//            {
//                this.bahtField = value;
//            }
//        }
//    }

//    /// <remarks/>
//    [System.SerializableAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
//    public partial class CustomsImportDeclarationGoodsShipmentDetailProductInfoIncreasedPrice
//    {

//        private object foreignField;

//        private object bahtField;

//        /// <remarks/>
//        public object Foreign
//        {
//            get
//            {
//                return this.foreignField;
//            }
//            set
//            {
//                this.foreignField = value;
//            }
//        }

//        /// <remarks/>
//        public object Baht
//        {
//            get
//            {
//                return this.bahtField;
//            }
//            set
//            {
//                this.bahtField = value;
//            }
//        }
//    }

//    /// <remarks/>
//    [System.SerializableAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
//    public partial class CustomsImportDeclarationGoodsShipmentDetailProductInfoCIFValue
//    {

//        private object foreignField;

//        private object bahtField;

//        private object assessField;

//        /// <remarks/>
//        public object Foreign
//        {
//            get
//            {
//                return this.foreignField;
//            }
//            set
//            {
//                this.foreignField = value;
//            }
//        }

//        /// <remarks/>
//        public object Baht
//        {
//            get
//            {
//                return this.bahtField;
//            }
//            set
//            {
//                this.bahtField = value;
//            }
//        }

//        /// <remarks/>
//        public object Assess
//        {
//            get
//            {
//                return this.assessField;
//            }
//            set
//            {
//                this.assessField = value;
//            }
//        }
//    }

//    /// <remarks/>
//    [System.SerializableAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
//    public partial class CustomsImportDeclarationGoodsShipmentDetailProductInfoPackage
//    {

//        private object amountField;

//        private string unitCodeField;

//        /// <remarks/>
//        public object Amount
//        {
//            get
//            {
//                return this.amountField;
//            }
//            set
//            {
//                this.amountField = value;
//            }
//        }

//        /// <remarks/>
//        public string UnitCode
//        {
//            get
//            {
//                return this.unitCodeField;
//            }
//            set
//            {
//                this.unitCodeField = value;
//            }
//        }
//    }

//    /// <remarks/>
//    [System.SerializableAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
//    public partial class CustomsImportDeclarationGoodsShipmentDetailReferenceDeclaration
//    {

//        private string numberField;

//        private object lineNumberField;

//        /// <remarks/>
//        public string Number
//        {
//            get
//            {
//                return this.numberField;
//            }
//            set
//            {
//                this.numberField = value;
//            }
//        }

//        /// <remarks/>
//        public object LineNumber
//        {
//            get
//            {
//                return this.lineNumberField;
//            }
//            set
//            {
//                this.lineNumberField = value;
//            }
//        }
//    }

//    /// <remarks/>
//    [System.SerializableAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
//    public partial class CustomsImportDeclarationGoodsShipmentDetailExciseQuantity
//    {

//        private object quantityField;

//        private string unitCodeField;

//        private object assessQuantityField;

//        /// <remarks/>
//        public object Quantity
//        {
//            get
//            {
//                return this.quantityField;
//            }
//            set
//            {
//                this.quantityField = value;
//            }
//        }

//        /// <remarks/>
//        public string UnitCode
//        {
//            get
//            {
//                return this.unitCodeField;
//            }
//            set
//            {
//                this.unitCodeField = value;
//            }
//        }

//        /// <remarks/>
//        public object AssessQuantity
//        {
//            get
//            {
//                return this.assessQuantityField;
//            }
//            set
//            {
//                this.assessQuantityField = value;
//            }
//        }
//    }

//    /// <remarks/>
//    [System.SerializableAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
//    public partial class CustomsImportDeclarationGoodsShipmentDetailArgumentativeTariff
//    {

//        private object codeField;

//        private object sequenceField;

//        private string privilegeCodeField;

//        private string reasonCodeField;

//        /// <remarks/>
//        public object Code
//        {
//            get
//            {
//                return this.codeField;
//            }
//            set
//            {
//                this.codeField = value;
//            }
//        }

//        /// <remarks/>
//        public object Sequence
//        {
//            get
//            {
//                return this.sequenceField;
//            }
//            set
//            {
//                this.sequenceField = value;
//            }
//        }

//        /// <remarks/>
//        public string PrivilegeCode
//        {
//            get
//            {
//                return this.privilegeCodeField;
//            }
//            set
//            {
//                this.privilegeCodeField = value;
//            }
//        }

//        /// <remarks/>
//        public string ReasonCode
//        {
//            get
//            {
//                return this.reasonCodeField;
//            }
//            set
//            {
//                this.reasonCodeField = value;
//            }
//        }
//    }

//    /// <remarks/>
//    [System.SerializableAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
//    public partial class CustomsImportDeclarationGoodsShipmentDetailModelInfo
//    {

//        private string numberField;

//        private object versionField;

//        private string companyTaxNumberField;

//        /// <remarks/>
//        public string Number
//        {
//            get
//            {
//                return this.numberField;
//            }
//            set
//            {
//                this.numberField = value;
//            }
//        }

//        /// <remarks/>
//        public object Version
//        {
//            get
//            {
//                return this.versionField;
//            }
//            set
//            {
//                this.versionField = value;
//            }
//        }

//        /// <remarks/>
//        public string CompanyTaxNumber
//        {
//            get
//            {
//                return this.companyTaxNumberField;
//            }
//            set
//            {
//                this.companyTaxNumberField = value;
//            }
//        }
//    }

//    /// <remarks/>
//    [System.SerializableAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
//    public partial class CustomsImportDeclarationGoodsShipmentDetailDuty
//    {

//        private string typeField;

//        private CustomsImportDeclarationGoodsShipmentDetailDutyRate rateField;

//        private CustomsImportDeclarationGoodsShipmentDetailDutyAmount amountField;

//        private object interestRateField;

//        private object monthField;

//        private object interestField;

//        private CustomsImportDeclarationGoodsShipmentDetailDutyArgumentative argumentativeField;

//        private CustomsImportDeclarationGoodsShipmentDetailDutyDeposit depositField;

//        /// <remarks/>
//        public string Type
//        {
//            get
//            {
//                return this.typeField;
//            }
//            set
//            {
//                this.typeField = value;
//            }
//        }

//        /// <remarks/>
//        public CustomsImportDeclarationGoodsShipmentDetailDutyRate Rate
//        {
//            get
//            {
//                return this.rateField;
//            }
//            set
//            {
//                this.rateField = value;
//            }
//        }

//        /// <remarks/>
//        public CustomsImportDeclarationGoodsShipmentDetailDutyAmount Amount
//        {
//            get
//            {
//                return this.amountField;
//            }
//            set
//            {
//                this.amountField = value;
//            }
//        }

//        /// <remarks/>
//        public object InterestRate
//        {
//            get
//            {
//                return this.interestRateField;
//            }
//            set
//            {
//                this.interestRateField = value;
//            }
//        }

//        /// <remarks/>
//        public object Month
//        {
//            get
//            {
//                return this.monthField;
//            }
//            set
//            {
//                this.monthField = value;
//            }
//        }

//        /// <remarks/>
//        public object Interest
//        {
//            get
//            {
//                return this.interestField;
//            }
//            set
//            {
//                this.interestField = value;
//            }
//        }

//        /// <remarks/>
//        public CustomsImportDeclarationGoodsShipmentDetailDutyArgumentative Argumentative
//        {
//            get
//            {
//                return this.argumentativeField;
//            }
//            set
//            {
//                this.argumentativeField = value;
//            }
//        }

//        /// <remarks/>
//        public CustomsImportDeclarationGoodsShipmentDetailDutyDeposit Deposit
//        {
//            get
//            {
//                return this.depositField;
//            }
//            set
//            {
//                this.depositField = value;
//            }
//        }
//    }

//    /// <remarks/>
//    [System.SerializableAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
//    public partial class CustomsImportDeclarationGoodsShipmentDetailDutyRate
//    {

//        private object valueField;

//        private object specificField;

//        private string specificUnitCodeField;

//        private object exemptionField;

//        /// <remarks/>
//        public object Value
//        {
//            get
//            {
//                return this.valueField;
//            }
//            set
//            {
//                this.valueField = value;
//            }
//        }

//        /// <remarks/>
//        public object Specific
//        {
//            get
//            {
//                return this.specificField;
//            }
//            set
//            {
//                this.specificField = value;
//            }
//        }

//        /// <remarks/>
//        public string SpecificUnitCode
//        {
//            get
//            {
//                return this.specificUnitCodeField;
//            }
//            set
//            {
//                this.specificUnitCodeField = value;
//            }
//        }

//        /// <remarks/>
//        public object Exemption
//        {
//            get
//            {
//                return this.exemptionField;
//            }
//            set
//            {
//                this.exemptionField = value;
//            }
//        }
//    }

//    /// <remarks/>
//    [System.SerializableAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
//    public partial class CustomsImportDeclarationGoodsShipmentDetailDutyAmount
//    {

//        private object amountField;

//        private object paidField;

//        private object depositField;

//        /// <remarks/>
//        public object Amount
//        {
//            get
//            {
//                return this.amountField;
//            }
//            set
//            {
//                this.amountField = value;
//            }
//        }

//        /// <remarks/>
//        public object Paid
//        {
//            get
//            {
//                return this.paidField;
//            }
//            set
//            {
//                this.paidField = value;
//            }
//        }

//        /// <remarks/>
//        public object Deposit
//        {
//            get
//            {
//                return this.depositField;
//            }
//            set
//            {
//                this.depositField = value;
//            }
//        }
//    }

//    /// <remarks/>
//    [System.SerializableAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
//    public partial class CustomsImportDeclarationGoodsShipmentDetailDutyArgumentative
//    {

//        private object valueRateField;

//        private object specificRateField;

//        private string specificUnitCodeField;

//        /// <remarks/>
//        public object ValueRate
//        {
//            get
//            {
//                return this.valueRateField;
//            }
//            set
//            {
//                this.valueRateField = value;
//            }
//        }

//        /// <remarks/>
//        public object SpecificRate
//        {
//            get
//            {
//                return this.specificRateField;
//            }
//            set
//            {
//                this.specificRateField = value;
//            }
//        }

//        /// <remarks/>
//        public string SpecificUnitCode
//        {
//            get
//            {
//                return this.specificUnitCodeField;
//            }
//            set
//            {
//                this.specificUnitCodeField = value;
//            }
//        }
//    }

//    /// <remarks/>
//    [System.SerializableAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
//    public partial class CustomsImportDeclarationGoodsShipmentDetailDutyDeposit
//    {

//        private string reasonCodeField;

//        private object amountField;

//        /// <remarks/>
//        public string ReasonCode
//        {
//            get
//            {
//                return this.reasonCodeField;
//            }
//            set
//            {
//                this.reasonCodeField = value;
//            }
//        }

//        /// <remarks/>
//        public object Amount
//        {
//            get
//            {
//                return this.amountField;
//            }
//            set
//            {
//                this.amountField = value;
//            }
//        }
//    }

//    /// <remarks/>
//    [System.SerializableAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
//    public partial class CustomsImportDeclarationGoodsShipmentDetailPermit
//    {

//        private string numberField;

//        private CustomsImportDeclarationGoodsShipmentDetailPermitIssueInfo issueInfoField;

//        /// <remarks/>
//        public string Number
//        {
//            get
//            {
//                return this.numberField;
//            }
//            set
//            {
//                this.numberField = value;
//            }
//        }

//        /// <remarks/>
//        public CustomsImportDeclarationGoodsShipmentDetailPermitIssueInfo IssueInfo
//        {
//            get
//            {
//                return this.issueInfoField;
//            }
//            set
//            {
//                this.issueInfoField = value;
//            }
//        }
//    }

//    /// <remarks/>
//    [System.SerializableAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
//    public partial class CustomsImportDeclarationGoodsShipmentDetailPermitIssueInfo
//    {

//        private object dateField;

//        private string authorityField;

//        /// <remarks/>
//        public object Date
//        {
//            get
//            {
//                return this.dateField;
//            }
//            set
//            {
//                this.dateField = value;
//            }
//        }

//        /// <remarks/>
//        public string Authority
//        {
//            get
//            {
//                return this.authorityField;
//            }
//            set
//            {
//                this.authorityField = value;
//            }
//        }
//    }




//}